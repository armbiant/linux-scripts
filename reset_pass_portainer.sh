#!/bin/bash
# shellcheck source=/dev/null

# Réinitialisation du mot de passe du compte admin de Portainer

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel du fichier de fonctions (qui fait lui-même appel au fichier de variable et à param.conf)
source $DOSSIER_INSTALL/fonctions.sh

echo -e "$COLTXT"
echo -e "Si vous poursuivez l'exécution de ce script, un nouveau mot de passe au compte admin de Portainer sera défini."
POURSUIVRE
echo -e "$COLCMD"

docker pull portainer/helper-reset-password
docker stop portainer-app
echo -e "$COLINFO"
docker run --rm -v e-combox_portainer_portainer-data:/data portainer/helper-reset-password
echo -e "$COLCMD"
docker start portainer-app