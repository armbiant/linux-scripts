#!/usr/bin/env bash
source /opt/e-comBox/param.conf

if [ -e /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer ]; then
   P_PASS=`cat /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer`
   else 
     P_PASS="portnairAdmin"
fi

P_USER="admin"
P_URL="http://localhost:$PORT_PORTAINER/portainer"
P_PRUNE="false"

if [ -z ${1+x} ]; then
  echo "Parameter #1 missing: stack name "
  exit 1
fi
TARGET="$1"

echo "Stop $TARGET"

echo "Tentative de connexion..."
P_TOKEN=$(curl -s -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"$P_USER\",\"password\":\"$P_PASS\"}" "$P_URL/api/auth")


if [[ $P_TOKEN = *"jwt"* ]]; then
  echo " ... success"
else
  echo "Impossible d'accéder à l'API de Portainer."
  exit 1
fi
T=$(echo $P_TOKEN | awk -F '"' '{print $4}')
echo "Token: $T"

INFO=$(curl -s -H "Authorization: Bearer $T" "$P_URL/api/endpoints/1/docker/info")
CID=$(echo "$INFO" | awk -F '"Cluster":{"ID":"' '{print $2}' | awk -F '"' '{print $1}')
echo "Cluster ID: $CID"

echo "Liste des stacks..."
STACKS=$(curl -s -H "Authorization: Bearer $T" "$P_URL/api/stacks")

echo "/---" && echo $STACKS && echo "\\---"

found=0
stack=$(echo "$STACKS"|jq --arg TARGET "$TARGET" -jc '.[]| select(.Name == $TARGET)')

if [ -z "$stack" ];then
  echo "Result: Stack not found."
  exit 1
fi
sid="$(echo "$stack" |jq -j ".Id")"
name=$(echo "$stack" |jq -j ".Name")

found=1
echo "Identified stack: $sid / $name"

existing_env_json="$(echo -n "$stack"|jq ".Env" -jc)"

echo "Stop stack..."
STOP=$(curl -s \
"$P_URL/api/stacks/$sid/stop" \
-X POST \
-H "Authorization: Bearer $T"
        )

echo "Contenu de STOP : $STOP"

if [ -z ${STOP+x} ]; then
  echo "Result: failure  to stop"
  exit 1
else
  echo "Result: successfully stop"
  exit 0
fi


if [ "$found" == "1" ]; then
  echo "Result: found stack but failed to process"
  exit 1
else
  echo "Result: fail"
  exit 1
fi
