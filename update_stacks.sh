#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel du fichier de fonctions (qui fait lui-même appel au fichier de variable et à param.conf)
source $DOSSIER_INSTALL/fonctions.sh

# Récupération sur gitlab du fichier de fonctions
curl -fsSL https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/"$BRANCHE"/fonctions.sh -o $DOSSIER_INSTALL/fonctions.sh

# Appel du fichier de fonctions
source $DOSSIER_INSTALL/fonctions.sh
#source ./fonctions.sh

echo -e "$COLINFO"
echo -e "Vous vous apprếtez à migrer les sites. Attention, la procédure peut être un peu longue."
echo -e "$COLCMD"
POURSUIVRE
UPDATE_STACKS

for image in $(docker images --filter=reference='reseaucerta/*:3.0' --format "{{.Repository}}:{{.Tag}}"); do
    docker rmi "$image"
done 2>>/var/log/ecombox-error.log

# Suppression des images inutilisées
if [ "$(docker images -qf dangling=true)" ]; then
    docker rmi "$(docker images -qf dangling=true)" 2>>/var/log/ecombox-error.log
fi
