#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel des fichiers de variables et de paramètres
source $DOSSIER_INSTALL/variables.sh
source $DOSSIER_INSTALL/param.conf

STOPPER() {
  echo -e "$COLSTOP"
  echo -e "Vous avez décidé de ne pas continuer à installer et configurer e-comBox. Vous pouvez reprendre la procédure quand vous voulez."
  echo -e "$COLTXT"
  exit 1
}

POURSUIVRE() {
  REPONSE=""
  while [ "$REPONSE" != "o" ] && [ "$REPONSE" != "O" ] && [ "$REPONSE" != "n" ]; do
    echo -e "$COLTXT"
    echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
    read -r REPONSE
    if [ -z "$REPONSE" ]; then
      REPONSE="o"
    fi
  done
  if [ "$REPONSE" != "o" ] && [ "$REPONSE" != "O" ]; then
    STOPPER
  fi
}

SAISIRMDP() {

  read -rs MDP
  while [ -z "$MDP" ]; do
    echo -e "$COLSTOP"
    echo -e "Le mot de passe que vous avez attribué à Portainer ne peut pas être vide !"
    echo -e "$COLSAISIE"
    echo -e "Saisissez le mot de passe."
    read -rs MDP
  done

  if [ -n "$MDP" ]; then
    echo -e "$COLSAISIE\n"
    echo -e "Saisissez de nouveau le mot de passe de Portainer."
    read -rs P_PASS
    echo -e "$COLDEFAUT"

    while [ "$MDP" != "$P_PASS" ]; do
      echo -e "$COLSTOP\n"
      echo -e "Les mots de passe ne correspondent pas. Il faut recommencer."
      echo -e "$COLSAISIE\n"
      echo -e "Saisissez le mot de passe."
      read -rs MDP
      echo -e "\nSaisissez de nouveau le mot de passe."
      read -rs P_PASS
    done
    #Échappement éventuel du mdp
    #P_PASS=$(echo "$MDP" | sed 's,\.\|\[\|\*\|\^\|\$\|/\|\\,\\&,g')
  fi
}

CONNECTE_API() {
  # Récupération des identifiants pour se connecter à l'API de Portainer

  # Test du mot de passe en comparaison avec le mdp par défaut (portnairAdmin)
  TEST_MDP_PORTAINER=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"admin\",\"password\":\"portnairAdmin\"}" "https://localhost:$PORT_PORTAINER/api/auth")

  # Si le test précédent renvoie un "Invalid credential", c'est que le mdp a été changé
  if (echo "$TEST_MDP_PORTAINER" | grep "Invalid credentials" &>/dev/null); then
    if [ -z "$P_PASS" ]; then
      echo -e "$COLSAISIE\n"
      echo -e "Saisissez le mot de passe du compte \"admin\" que vous avez attribué à Portainer (c'est normal que les lettres/chiffres/symboles saisis n'apparaissent pas)."
      SAISIRMDP
    fi
  else
    P_PASS="portnairAdmin"
  fi

  P_USER="admin"
  P_URL="https://localhost:$PORT_PORTAINER"
  P_PRUNE="false"

  # Récupération du jeton de connexion à l'API de Portainer

  echo "Tentative de connexion à l'API..."
  echo "Tentative de connexion à l'API..." >>/var/log/ecombox.log
  # Ajout du -k pour que les certificats ne soient pas vérifiés
  P_TOKEN=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"$P_USER\",\"password\":\"$P_PASS\"}" "$P_URL/api/auth")

  if [[ "$P_TOKEN" = *"jwt"* ]]; then
    echo " ... success"
    echo ""
    echo " ... success" >>/var/log/ecombox.log
    echo ""
  elif echo "$P_TOKEN" | grep "Invalid credentials"; then
    echo "Impossible d'accéder à l'API de Portainer. Le mot de passe que vous saisissez pour Portainer est erroné. Réinitialisez-le si besoin via le script reset_pass_portainer.sh."
    echo ""
    echo "Impossible d'accéder à l'API de Portainer. Le mot de passe que vous saisissez pour Portainer est erroné. Réinitialisez-le si besoin." >>/var/log/ecombox-error.log
    echo ""
    exit 1
  else
    echo "Impossible d'accéder à l'API de Portainer. Veuillez contacter votre administrateur."
    echo ""
    echo "Impossible d'accéder à l'API de Portainer. Veuillez contacter votre administrateur." >>/var/log/ecombox-error.log
    echo ""
    exit 1
  fi

  T=$(echo "$P_TOKEN" | awk -F '"' '{print $4}')
  #echo "Token: $T"

}

STOP_STACKS() {
  # Connexion à l'API de Portainer
  CONNECTE_API

  # Récupération des stacks
  echo "Récupération des stacks..." >>/var/log/ecombox.log
  echo ""
  STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")

  echo "$STACKS" >"$DOSSIER_INSTALL"/stacks.json

  if [ -n "$STACKS" ]; then
    cat $DOSSIER_INSTALL/stacks.json | jq -c '.[] |{Id,Name,Status}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')
      status_stack=$(echo "$line" | jq -r '.Status')

      # Arrêt du stack si celui-ci n'est pas déjà arrếté
      if [ "$status_stack" = "1" ]; then
        echo -e "$COLCMD"
        echo "Arrêt du stack $nom_stack..."
        echo "Arrêt du stack $nom_stack..." >>/var/log/ecombox.log
        STOP=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$num_stack/stop" \
            -X POST \
            -H "Authorization: Bearer $T"
        )
        echo -e "$COLINFO"
        echo "Retour de l'arrêt : $STOP" >>/var/log/ecombox.log
      else
        echo "Le stack $nom_stack est déjà arrêté." >>/var/log/ecombox.log
      fi
    done
  fi

}

START_STACKS() {
  # Connexion à l'API de Portainer
  CONNECTE_API

  # Récupération des stacks
  echo "Récupération des stacks..." >>/var/log/ecombox.log
  echo ""
  STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")

  echo "$STACKS" >"$DOSSIER_INSTALL"/stacks.json

  if [ -n "$STACKS" ]; then
    cat $DOSSIER_INSTALL/stacks.json | jq -c '.[] |{Id,Name,Status}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')
      status_stack=$(echo "$line" | jq -r '.Status')

      # Démarrage du stack si celui-ci n'est pas déjà démarré
      if [ "$status_stack" = "0" ]; then
        echo -e "$COLCMD"
        echo "Démarrage du stack $nom_stack..."
        echo "Démarrage du stack $nom_stack..." >>/var/log/ecombox.log
        START=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$num_stack/start" \
            -X POST \
            -H "Authorization: Bearer $T"
        )
        echo -e "$COLINFO"
        echo "Retour du démarrage : $START" >>/var/log/ecombox.log
      else
        echo "Le stack $nom_stack est déjà démarré." >>/var/log/ecombox.log
      fi
    done
  fi

}

UPDATE_STACKS() {
  if [ ! -d "$DOSSIER_MIGRATION" ]; then
    mkdir "$DOSSIER_MIGRATION"
  fi

  DATE=$(date '+%Y-%m-%d-%H%M')

  echo -e "$COLPARTIE"
  echo -e "Migration des sites... Cela peut prendre du temps..."
  echo -e ""

  echo -e "Migration des sites le $DATE" >"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
  echo -e "$COLCMD"

  # Récupération des docker-compose
  cd "$DOSSIER_MIGRATION" || exit

  if [ -d "$DOSSIER_MIGRATION"/e-comBox_docker-compose/ ]; then
    rm -rf "$DOSSIER_MIGRATION"/e-comBox_docker-compose/
    git clone -b migration-v4 https://gitlab.com/e-combox/e-comBox_docker-compose.git
    rm -rf "$DOSSIER_MIGRATION"/e-comBox_docker-compose/.git
  else
    git clone -b migration-v4 https://gitlab.com/e-combox/e-comBox_docker-compose.git
    rm -rf "$DOSSIER_MIGRATION"/e-comBox_docker-compose/.git
  fi

  # Connexion à l'API de Portainer
  CONNECTE_API

  INFO=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/endpoints/1/docker/info")
  CID=$(echo "$INFO" | awk -F '"Cluster":{"ID":"' '{print $2}' | awk -F '"' '{print $1}')

  {
    echo -e "Cluster ID: $CID"

    # Récupération des stacks
    echo -e "Récupération des stacks...\n"
  } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

  STACKS=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks")

  echo "$STACKS" >"$DOSSIER_MIGRATION"/stacks_"$DATE".json

  if [ -n "$STACKS" ]; then
    # shellcheck disable=SC2002
    cat "$DOSSIER_MIGRATION"/stacks_"$DATE".json | jq -c '.[] |{Id,Name,EntryPoint,Env}' | while read -r line; do
      num_stack=$(echo "$line" | jq -r '.Id')
      nom_stack=$(echo "$line" | jq -r '.Name')
      dc_yml=$(echo "$line" | jq -r '.EntryPoint')
      env_json=$(echo "$line" | jq -jc '.Env')

      # Modification du nom des docker-compose "custom" qui intégre la référence à une image comportant les nouveaux scripts
      dc_yml=$(echo "$dc_yml" | sed -e 's/custom/neutre/')

      # Modification du nom des docker-compose "kanboard" pour le remplacer par kanboard-vierge (nom du DC a changé entre la v3 et la v4)
      dc_yml=$(echo "$dc_yml" | sed -e 's/docker-compose-kanboard.yml/docker-compose-kanboard-vierge.yml/')

      # Modification du nom des docker-compose "Mautic" pour le remplacer par mautic-v3
      dc_yml=$(echo "$dc_yml" | sed -e 's/docker-compose-mautic.yml/docker-compose-mautic-v3.yml/')

      # Modification du port du registry dans la variable env_json
      env_json=$(echo "$env_json" | jq --arg REGISTRY_PORT "$PORT_REGISTRY" '(.[] | select(.name == "REGISTRY_PORT")).value |= $REGISTRY_PORT')

      {
        echo -e "\nNom du stack traité : $nom_stack"
        echo -e "\nLa variable dc_yml est : $dc_yml"
        echo -e "\nLa variable d'env est $env_json."
      } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      #done
      #fi
      #exit 1

      # Démarrage du stack
      #echo -e "$COLCMD"
      #echo "Démarrage du stack $nom_stack..."
      #START=$(
      #curl --noproxy "*" -s -k \
      #"$P_URL/api/stacks/$num_stack/start" \
      #-X POST \
      #-H "Authorization: Bearer $T"
      #)
      #echo -e "$COLINFO"
      #echo "Démarrage du stack $nom_stack..." >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
      #echo "Retour du démarrage : $START" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
      #echo -e "$COLCMD"

      # Mise à jour du stack
      echo -e "$COLCMD"
      echo "Mise à jour du stack $nom_stack..."
      TARGET_YML="$DOSSIER_MIGRATION/e-comBox_docker-compose/$dc_yml"

      dcompose=$(cat "$TARGET_YML")
      dcompose="${dcompose//$'\r'/''}"
      dcompose="${dcompose//$'"'/'\"'}"

      {
        echo -e "/-----Lecture du fichier YML--------"
        echo -e "$dcompose"
        echo -e "\---------------------"
      } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      dcompose="${dcompose//$'\n'/'\n'}"
      data_prefix="{\"Id\":\"$num_stack\",\"StackFileContent\":\""
      data_suffix="\",\"Env\":$env_json,\"Prune\":$P_PRUNE}"

      {
        echo "/~~~~Conversion du JSON~~~~~~"
        echo "$data_prefix$dcompose$data_suffix"
        echo "\~~~~~~~~~~~~~~~~~~~~~~~~"
      } >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      echo "$data_prefix$dcompose$data_suffix" >json"$num_stack".tmp

      echo "Mise à jour du stack $nom_stack..." >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
      UPDATE=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/stacks/$num_stack?endpointId=1" \
          -X PUT \
          -H "Authorization: Bearer $T" \
          -H "Content-Type: application/json;charset=UTF-8" \
          -H 'Cache-Control: no-cache' \
          --data-binary "@json$num_stack.tmp"
      )
      #rm json.tmp

      echo -e "Retour de la mise à jour : $UPDATE" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

      # Ajout (ou modification) de la restriction (le stack doit appartenir au groupe "Profs")
      echo -e "$COLCMD"
      echo -e "Le stack $nom_stack devient la propriété du groupe \"Profs\"."
      echo -e "Ainsi, tous les utilisateurs appartenant à ce groupe pourront agir sur les sites."
      echo -e "L'affectation des sites à un utilisateur en particulier se fait sur Portainer."

      RESTRICTION_STACK=$(curl --noproxy "*" -s -k \
      "$P_URL/api/stacks/$num_stack" \
      -X GET \
      -H "Authorization: Bearer $T" | grep ResourceControl)

      if [ -z "$RESTRICTION_STACK" ]; then
        AJOUT_RESTRICTION_STACK=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls" \
          -X POST \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6,\"resourceId\":\"1_$nom_stack\"}"
      )

      echo -e "Retour AJOUT_RESTRICTION_STACK : $AJOUT_RESTRICTION_STACK" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
      
      else
      # Récupération de l'ID de restriction
      ID_RESTRICTION_STACK=$(echo "$RESTRICTION_STACK" | jq -r '.ResourceControl.Id')

      # Mise à jour de la restriction
      MAJ_RESTRICTION_STACK=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/resource_controls/$ID_RESTRICTION_STACK" \
            -X PUT \
            -H "Authorization: Bearer $T" \
            -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6}"
      )
      echo -e "Retour MAJ_RESTRICTION_STACK : $MAJ_RESTRICTION_STACK" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log

   fi

      # Arrêt du stack sauf le stack FSserver
      if [ ! "$num_stack" = "$ID_STACK_FSSERVER" ]; then
        echo -e "$COLCMD"
        echo -e "Arrêt du stack $nom_stack..."
        STOP=$(
          curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$num_stack/stop" \
            -X POST \
            -H "Authorization: Bearer $T"
        )
        echo -e "$COLINFO"
        echo "Arrêt du stack $nom_stack..." >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
        echo "Retour de l'arrêt : $STOP" >>"$DOSSIER_MIGRATION"/rapport_migration_"$DATE".log
      fi
    done
    echo -e "$COLCMD"
  fi

}

GESTION_PROFS() {
  # Connexion à l'API de Portainer
  CONNECTE_API

  # Création de l'équipe "Profs"
  echo -e "$COLDEFAUT"
  echo -e "Création éventuelle de l'équipe \"Profs\""
  echo -e "Création éventuelle de l'équipe \"Profs\"" >>/var/log/ecombox.log
  GROUPE_PROFS=$(
    curl --noproxy "*" -s -k \
      "$P_URL/api/teams" \
      -X POST \
      -H "Authorization: Bearer $T" \
      -d "{\"name\":\"profs\"}"
  )

  if (echo "$GROUPE_PROFS" | grep "Team already exists" &>/dev/null); then

    echo -e "$COLINFO"
    echo -e "L'équipe \"Profs\" existe déjà."
    echo -e "L'équipe \"Profs\" existe déjà." >>/var/log/ecombox.log
    # Récupération de l'ID Prof
    GROUPE_PROFS=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/teams" \
        -X GET \
        -H "Authorization: Bearer $T" | grep "profs" | tr -d "[]"
    )
    ID_GROUPE_PROFS=$(echo "$GROUPE_PROFS" | jq -r '.Id')
    echo -e "$COLDEFAUT"
  else
    if [ "$(echo "$GROUPE_PROFS" | jq -r '.Name')" = "profs" ]; then
      echo -e "$COLINFO"
      echo -e "L'équipe \"Profs\" a été créée."
      echo -e "L'équipe \"Profs\" a été créée" >>/var/log/ecombox.log
      # Récupération de l'ID Prof
      ID_GROUPE_PROFS=$(echo "$GROUPE_PROFS" | jq -r '.Id')
      echo -e "$COLDEFAUT"
    else
      echo -e "$COLERREUR"
      echo -e "L'équipe \"Profs\" n'a pa pu être créée. Consultez \"/var/log/ecombox-error.log\"".
      echo -e "$GROUPE_PROFS \n L'équipe \"Profs\" n'a pas pu être créée" >>/var/log/ecombox-error.log
      echo -e "$COLDEFAUT"
    fi
  fi

  if [ -n "$ID_GROUPE_PROFS" ]; then
    echo -e "Ajout des permissions"
    echo -e "Ajout des permissions" >>/var/log/ecombox.log

    # Accès à l'équipe "profs" à l'environnement "primary" dans tous les cas car on ne sait jamais :)
    AJOUT_PERMISSIONS=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/endpoints/{1}" \
        -X PUT \
        -H "Authorization: Bearer $T" \
        -d "{\"TeamAccessPolicies\":{\"$ID_GROUPE_PROFS\":{\"RoleId\":0}}}"
    )
    echo -e "Retour AJOUT_PERMISSIONS : $AJOUT_PERMISSIONS" >>/var/log/ecombox.log

    # On fait la suite dans tous les cas car le serveur nginx est forcément nouvellement créé
    # Restriction sur Nginx
    # Récupération de l'ID du container nginx
    CONTENEUR_NGINX=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/endpoints/1/docker/containers/nginx/json" \
        -X GET \
        -H "Authorization: Bearer $T"
    )
    ID_CONTENEUR_NGINX=$(echo "$CONTENEUR_NGINX" | jq -r '.Id')

    echo -e "Retour ID_CONTENEUR_NGINX : $ID_CONTENEUR_NGINX" >>/var/log/ecombox.log

    # Création d'une nouvelle restriction si aucune restriction n'existe
    RESTRICTION_NGINX=$(curl --noproxy "*" -s -k \
      "$P_URL/api/endpoints/1/docker/containers/nginx/json" \
      -X GET \
      -H "Authorization: Bearer $T" | grep ResourceControl)

    if [ -z "$RESTRICTION_NGINX" ]; then
      AJOUT_RESTRICTION_NGINX=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls" \
          -X POST \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1,\"resourceId\":\"$ID_CONTENEUR_NGINX\"}"
      )

      echo -e "Retour AJOUT_RESTRICTION_NGINX : $AJOUT_RESTRICTION_NGINX" >>/var/log/ecombox.log

    else
      # Récupération de l'ID de restriction
      ID_RESTRICTION_NGINX=$(echo "$RESTRICTION_NGINX" | jq -r '.Portainer.ResourceControl.Id')
      # Mise à jour de la restriction
      MAJ_RESTRICTION_NGINX=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls/$ID_RESTRICTION_NGINX" \
          -X PUT \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1}"
      )
      echo -e "Retour MAJ_RESTRICTION_NGINX : $MAJ_RESTRICTION_NGINX" >>/var/log/ecombox.log

    fi

    # Restriction sur le Registry
    # Récupération de l'ID du container registry
    CONTENEUR_REGISTRY=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/endpoints/1/docker/containers/e-combox_registry/json" \
        -X GET \
        -H "Authorization: Bearer $T"
    )
    ID_CONTENEUR_REGISTRY=$(echo "$CONTENEUR_REGISTRY" | jq -r '.Id')

    # Création d'une nouvelle restriction si aucune restriction n'existe
    RESTRICTION_REGISTRY=$(curl --noproxy "*" -s -k \
      "$P_URL/api/endpoints/1/docker/containers/e-combox_registry/json" \
      -X GET \
      -H "Authorization: Bearer $T" | grep ResourceControl)

    if [ -z "$RESTRICTION_REGISTRY" ]; then
      AJOUT_RESTRICTION_REGISTRY=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls" \
          -X POST \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1,\"resourceId\":\"$ID_CONTENEUR_REGISTRY\"}"
      )

      echo -e "Retour AJOUT_RESTRICTION_REGISTRY : $AJOUT_RESTRICTION_REGISTRY" >>/var/log/ecombox.log

    else
      # Récupération de l'ID de restriction
      ID_RESTRICTION_REGISTRY=$(echo "$RESTRICTION_REGISTRY" | jq -r '.Portainer.ResourceControl.Id')
      # Mise à jour de la restriction
      MAJ_RESTRICTION_REGISTRY=$(
        curl --noproxy "*" -s -k \
          "$P_URL/api/resource_controls/$ID_RESTRICTION_REGISTRY" \
          -X PUT \
          -H "Authorization: Bearer $T" \
          -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":1}"
      )
      echo -e "Retour MAJ_RESTRICTION_REGISTRY : $MAJ_RESTRICTION_REGISTRY" >>/var/log/ecombox.log

    fi

    echo -e "Retour ID_RESTRICTION_REGISTRY: $ID_RESTRICTION_REGISTRY" >>/var/log/ecombox.log
  fi
}

STOP_SERVICES() {

  # Arrêt des sites
  echo -e "$COLCMD"
  echo - e "Arrêt des sites encore actifs..."
  STOP_STACKS

  # Arrêt de tous les services
  echo -e "Arrêt des services…"

  # Reverse proxy
  cd $DOSSIER_INSTALL/e-comBox_reverseproxy || exit
  docker-compose down

  # Registry
  docker stop e-combox_registry

  # Serveur git
  docker stop e-combox_gitserver

  # Portainer
  cd $DOSSIER_INSTALL/e-comBox_portainer || exit
  docker-compose down
  docker volume rm e-combox_portainer_portainer-data

  # E-combox
  docker stop e-combox
}

START_SERVICES() {

  # Redémarrage de tous les services
  echo -e "$COLINFO"
  echo -e "Redémarrage des services…"
  echo -e "$COLCMD"
  # Reverse proxy
  cd $DOSSIER_INSTALL/e-comBox_reverseproxy || exit
  docker-compose up -d

  # Registry
  docker start e-combox_registry

  # Serveur git local
  docker start e-combox_gitserver

  # Portainer
  cd $DOSSIER_INSTALL/e-comBox_portainer || exit
  docker-compose up -d

  # E-combox
  docker start e-combox

  # FSserver
  CONNECTE_API
  STACK_FSSERVER=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks" | jq -c '.[] | select(.Name == "fsserver")')
  ID_STACK_FSSERVER=$(echo "$STACK_FSSERVER" | jq -r '.Id')
  if [ -n "$STACK_FSSERVER" ]; then
    echo -e "Démarrage du stack \"FSserver\"..."
    echo -e "\nDémarrage du stack \"FSserver\"..." >>/var/log/ecombox.log
    START_STACK_FSSERVER=$(
      curl --noproxy "*" -s -k \
        "$P_URL/api/stacks/$ID_STACK_FSSERVER/start" \
        -X POST \
        -H "Authorization: Bearer $T"
    )
    echo -e "$COLINFO"
    echo -e "Retour du démarrage du stack FSserver: $START_STACK_FSSERVER" >>/var/log/ecombox.log
  else
    echo -e "$COLINFO"
    echo -e "Le stack FSserver n'existe pas"
    echo -e "$COLCMD"
  fi
}

SAUV_SITES() {
  if [ -e "$DOSSIER_INSTALL/version3" ]; then
    NOM_ARCHIVE=sitesV3.tar.gz
  else
    # Choix du nom de l'archive
    DATE=$(date '+%Y-%m-%d')
    NOM_ARCHIVE_PROPOSEE=sitesV4_$DATE.tar.gz
    echo -e "\nValidez le nom de l'archive de sauvegarde proposée, sinon saisissez un autre nom (l'extension doit obligatoirement être en .tar.gz) : ${COLCHOIX}$NOM_ARCHIVE_PROPOSEE"
    read -r NOM_ARCHIVE_SAISIE
    if [ -z "$NOM_ARCHIVE_SAISIE" ]; then
      NOM_ARCHIVE="$NOM_ARCHIVE_PROPOSEE"
    else
      NOM_ARCHIVE="$NOM_ARCHIVE_SAISIE"
    fi
  fi

  # Choix du dossier de sauvegarde
  echo -e "$COLTXT"
  echo -e "L'archive $NOM_ARCHIVE sera stockée par défaut dans le dossier $DOSSIER_INSTALL/sauv."
  echo -e "Voulez-vous choisir un autre dossier ? (n par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
  read -r REPONSE_DOSSIER
  if [ -z "$REPONSE_DOSSIER" ] || [ "$REPONSE_DOSSIER" = "n" ] || [ "$REPONSE_DOSSIER" = "N" ]; then
    REPONSE_DOSSIER="n"
    DEST_SAUV=$DOSSIER_INSTALL/sauv
    if [ ! -d "/$DOSSIER_INSTALL/sauv" ]; then
      mkdir $DOSSIER_INSTALL/sauv
    fi
  fi
  if [ "$REPONSE_DOSSIER" != "n" ] && [ "$REPONSE_DOSSIER" != "n" ]; then
    # Parcours des dossiers accessibles
    if (! dialog 2>/dev/null); then
      apt update
      apt install dialog
    fi
    DEST_SAUV=$(dialog --stdout --title "Choisissez un dossier. Le chemin vers le dossier voulu peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect / 14 48)
    # Validation du dossier en gérant un éventuel "annuler"
    if [ -z "$DEST_SAUV" ]; then
      echo -e "\nVous n'avez pas choisi de dossier de sauvegarde. Veuillez relancez le script pour sélectionner un nouveau dossier de sauvegarde."
      sleep 2
      exit
    else
      echo -e "\nVous avez choisi le dossier ${DEST_SAUV}. Validez-vous ce choix ? (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
      read -r REPONSE_DEST
      if [ -z "$REPONSE_DEST" ]; then
        REPONSE_DEST="o"
      fi
      if [ "$REPONSE_DEST" != "o" ] && [ "$REPONSE_DEST" != "0" ]; then
        echo -e "\nVeuillez relancez le script pour sélectionner un nouveau dossier de sauvegarde."
        sleep 2
        exit
      fi
    fi
  fi

  # Arrêt des services
  STOP_SERVICES

  # Sauvegarde des sites
  echo -e "$COLINFO"
  echo -e "\nLa sauvegarde des sites est en cours, une archive de sauvegarde $NOM_ARCHIVE sera créée dans $DEST_SAUV, merci de patientez…"

  cd /var/lib/docker/ || exit
  tar -czf "$DEST_SAUV/$NOM_ARCHIVE" --preserve-permissions --same-owner volumes 2>>/"$DEST_SAUV"/sauv.log

  echo -e "\nLa sauvegarde des sites est terminée. L'archive de sauvegarde $NOM_ARCHIVE a été créée dans $DEST_SAUV. En cas de problème, voir la procédure de restauration dans la documentation."
  echo -e "$COLCMD"

  # redémarrage des services
  START_SERVICES
}

VALIDE_ARCHIVE() {
  # Validation de l'archive en gérant un éventuel "annuler"
  if [ -z "$ARCHIVE" ]; then
    echo -e "\nVous n'avez pas choisi d'archive à restaurer. Veuillez relancez le script pour sélectionner une archive à restaurer."
    sleep 2
    exit
  else
    echo -e "\nVous avez choisi l'archive ${ARCHIVE}. Validez-vous ce choix ? (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
    read -r REPONSE_VALIDE_ARCHIVE
    if [ -z "$REPONSE_VALIDE_ARCHIVE" ]; then
      REPONSE_VALIDE_ARCHIVE="o"
    fi
    if [ ! "$REPONSE_VALIDE_ARCHIVE" = "o" ] && [ ! "$REPONSE_VALIDE_ARCHIVE" = "O" ]; then
      echo -e "\nVeuillez relancez le script pour sélectionner une archive à restaurer."
      sleep 2
      exit
    fi
  fi
}

RESTAURE_SITES() {

  # Restauration des sites

  # Choix de l'archive à restaurer
  echo -e "$COLTXT"
  echo -e "L'archive est stockée par défaut dans le dossier $DOSSIER_INSTALL/sauv."
  echo -e "Voulez-vous choisir un autre dossier ? (n par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
  read -r REPONSE_DOSSIER
  if [ -z "$REPONSE_DOSSIER" ] || [ "$REPONSE_DOSSIER" = "n" ] || [ "$REPONSE_DOSSIER" = "N" ]; then
    REPONSE_DOSSIER="n"
    DOSSIER_SOURCE=$DOSSIER_INSTALL/sauv
    if (! dialog 2>/dev/null); then
      apt update
      apt install dialog
    fi
    ARCHIVE=$(dialog --stdout --title "Choisissez l'archive à partir de laquelle vous voulez restaurer les sites. Le nom de l'archive peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect $DOSSIER_SOURCE/ 14 48)
    VALIDE_ARCHIVE
    echo "Le nom de l'archive est $ARCHIVE"
  fi
  if [ "$REPONSE_DOSSIER" != "n" ] && [ "$REPONSE_DOSSIER" != "n" ]; then
    # Parcours des dossiers accessibles
    ARCHIVE=$(dialog --stdout --title "Choisissez l'archive à partir de laquelle vous voulez restaurer les sites. Le chemin vers le dossier voulu peut directement être saisi ou peut être choisi en utilisant les flèches et la touche de tabulation du clavier. Un élément est validé avec la barre d'espacement." --fselect / 14 48)
    # Validation de l'archive en gérant un éventuel "annuler"
    if [ -z "$ARCHIVE" ]; then
      echo -e "\nVous n'avez pas choisi d'archive à restaurer. Veuillez relancez le script pour sélectionner une archive à restaurer."
      sleep 2
      exit
    else
      echo -e "\nVous avez choisi l'archive ${ARCHIVE}. Validez-vous ce choix ? (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
      read -r REPONSE_ARCHIVE
      if [ -z "$REPONSE_ARCHIVE" ]; then
        REPONSE_ARCHIVE="o"
      fi
      if [ "$REPONSE_ARCHIVE" != "o" ] && [ "$REPONSE_ARCHIVE" != "O" ]; then
        echo -e "\nVeuillez relancez le script pour sélectionner une archive à restaurer."
        sleep 2
        exit
      fi
    fi
  fi

  # Arrêt de tous les services
  STOP_SERVICES

  echo -e "La restauration des sites est en cours, merci de patientez…"

  # Voir s'il vaut mieux purger tous les volumes avec une commande docker
  cd /var/lib/docker/ || exit
  rm -rf volumes
  tar -xzf "$ARCHIVE" --preserve-permissions --same-owner >>/var/log/ecombox.log

  # Gérer les erreurs

  # Redémarrage de tous les services
  echo -e "Redémarrage des services…"

  START_SERVICES

  echo -e "$COLINFO"
  echo -e "\nLa restauration des sites est terminée. Vous pouvez relancer l'e-comBox après avoir vidé le cache de votre navigateur. Attention, si vous restaurez sur un autre environnement, il est également nécessaire de reconfigurer l'application."
  echo -e "$COLCMD"
}

USE_CERTIFICAT() {
  # Mise en place des certificats dans nginx
  cp "$CHEMIN_CERT" /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt
  cp "$CHEMIN_KEY" /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key

  # Sauvegarde des certificats dans $DOSSIER_CERTS pour réutilisation
  cp /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt "$DOSSIER_CERTS"/ecombox.crt
  cp /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key "$DOSSIER_CERTS"/ecombox.key

  echo -e "$COLINFO"
  echo -e "Votre certificat a été installé"
  echo -e "$COLTXT"
}

CREATE_CERTIFICAT() {
  test_certificat=$(cat $DOSSIER_INSTALL/param.conf | grep -v '#' | grep CODE_PAYS)

  if [ -z "$test_certificat" ]; then
    {
      echo -e "\n# Les 4 variables qui suivent sont utiles pour créer un certificat auto signé"
      echo -e "# Si vous avez déjà un certificat, saisir uniquement les valeurs des deux variables suivantes"
      echo -e "\n# Code Pays sur 2 lettres\nCODE_PAYS=FR"
      echo -e "\n# Nom Pays\nNOM_PAYS=France"
      echo -e "\n# # Nom région\nNOM_REGION=\"Corse\""
      echo -e "\n# # Nom organisation\nNOM_ORGANISATION=\"Reseau Certa\""
      echo -e "\n# Les 2 variables qui suivent sont utiles pour donner le chemin vers les élémnents pour mettre en place un certificat existant"
      echo -e "\n# Fichier certificat pour nginx existant avec le chemin complet /chemin/fichier.crt\nCHEMIN_CERT="
      echo -e "\n# Fichier de la clé privée pour nginx existante avec le chemin complet /chemin/fichier.key\nCHEMIN_KEY="
    } >>$DOSSIER_INSTALL/param.conf
  fi

  # Recharge du fichier de paramètres
  source $DOSSIER_INSTALL/param.conf

  # Certificat auto-signé ou certificat perso

  if [ -z "$CHEMIN_CERT" ] || [ -z "$CHEMIN_KEY" ]; then
    echo -e "$COLINFO"
    echo -e "Le système constate qu'aucun chemin vers un certificat existant n'est fourni dans le fichier param.conf. Un certificat auto-signé va être créé si vous confirmez ce choix sinon les chemins vers les fichiers nécessaires vous seront demandés.${COLTXT}"
    echo -e "Confirmez-vous ce choix (o par défaut pour le certificat auto-signé) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
    read -r REPONSE_CERT
    if [ -z "$REPONSE_CERT" ]; then
      REPONSE_CERT="o"
    fi

    if [ "$REPONSE_CERT" != "o" ]; then
      echo -e "$COLTXT"
      echo -e "Saisissez le chemin et le nom de votre certificat (/chemin/nom_fichier.crt) : ${COLSAISIE}\c"
      read -r CHEMIN_CERT
      # Boucle pour vérifier l'accessibilité du fichier (à faire)
      echo -e "$COLTXT"
      echo -e "Saisissez le chemin et le nom de votre clé privée (/chemin/nom_fichier.key) : ${COLSAISIE}\c"
      read -r CHEMIN_KEY
      # Boucle pour vérifier l'accessibilité du fichier (à faire)
      # Ajout des valeurs dans param.conf
      sed -i "s|^CHEMIN_CERT=|CHEMIN_CERT=$CHEMIN_CERT|g" $DOSSIER_INSTALL/param.conf
      sed -i "s|^CHEMIN_KEY=|CHEMIN_KEY=$CHEMIN_KEY|g" $DOSSIER_INSTALL/param.conf
      USE_CERTIFICAT

    else
      # Création du certificat auto-signé
      echo -e "$COLINFO"
      echo -e "Le système va créer un certificat auto-signé avec les valeurs suivantes."
      echo -e "Code Pays : ${COLCHOIX}$CODE_PAYS"
      echo -e "$COLINFO"
      echo -e "Nom pays : ${COLCHOIX}$NOM_PAYS"
      echo -e "$COLINFO"
      echo -e "Nom Région : ${COLCHOIX}$NOM_REGION"
      echo -e "$COLINFO"
      echo -e "Nom organisation : ${COLCHOIX}$NOM_ORGANISATION"
      echo -e "$COLINFO"
      if [ -n "$DOMAINE" ]; then
        echo -e "Nom du serveur : ${COLCHOIX}$DOMAINE"
        CN=$DOMAINE
      else
        echo -e "Adresse IP privée : ${COLCHOIX}$ADRESSE_IP_PRIVEE"
        CN=$ADRESSE_IP_PRIVEE
      fi
      echo -e "$COLTXT"
      echo -e "Si ces valeurs ne conviennent pas, vous pourrez les modifier en éditant le fichier $DOSSIER_INSTALL/param.conf puis en lançant le script \"/opt/e-comBox/update_certificat.sh\" pour mettre à jour le certificat."
      sleep 5
      openssl req -new -subj "/C=$CODE_PAYS/ST=$NOM_PAYS/L=$NOM_REGION/O=$NOM_ORGANISATION/CN=$CN" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt -keyout /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key

      echo -e "$COLINFO"
      echo -e "Le certificat auto-signé a été créé et installé"
      echo -e "$COLTXT"
    fi

  else
    echo -e "$COLINFO"
    echo -e "Le système constate qu'un chemin vers un certificat existant est fourni dans le fichier param.conf.${COLTXT}"
    echo -e "Confirmez-vous ce choix (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
    read -r REPONSE_CERT
    if [ -z "$REPONSE_CERT" ]; then
      REPONSE_CERT="o"
    fi
    if [ "$REPONSE_CERT" = "o" ]; then
      USE_CERTIFICAT
    fi
  fi

  # Reload conf nginx
  docker exec nginx nginx -s reload
}
