#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Synchronisation du nouveau mot de passe de Portainer avec la e-comBox

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel du fichier de fonctions (qui fait lui-même appel au fichier de variable et à param.conf)
source $DOSSIER_INSTALL/fonctions.sh

SAISIRMDP()
{
  
read -rs MDP
if [ -n "$MDP" ]; then
   echo -e "$COLSAISIE\n"
   echo -e "Saisissez de nouveau le mot de passe." 
   read -rs MDPbis
   
   while [ "$MDP" != "$MDPbis" ]
   do
     echo -e "$COLSTOP\n" 
     echo -e "Les mots de passe ne correspondent pas. Il faut recommencer."
     echo -e "$COLSAISIE\n" 
     echo -e "Saisissez le mot de passe."
     read -rs MDP
     echo ""
     echo -e "Saisissez de nouveau le mot de passe." 
     read -rs MDPbis
   done
   #Échappement éventuel du mdp
   MDP_E=$(echo "$MDP" | sed 's,\.\|\[\|\*\|\^\|\$\|/\|\\,\\&,g')
  fi
}


echo -e "$COLTITRE"
echo "********************************************************************************"
echo "*       CONFIGURATION DE LA SYNCHRONISATION DU MOT DE PASSE DE PORTAINER       *"
echo "********************************************************************************"

echo -e "$COLDEFAUT\n"

            
echo -e "Vous venez de modifier le mot de passe \"admin\" de Portainer. Ce script va le synchroniser avec la e-comBox. $COLSAISIE\n"
echo -e "Saisissez le mot de passe (le mot de passe saisi ne s'affiche pas) ou laisser vide pour ne pas synchroniser."
SAISIRMDP

echo -e "$COLCMD"

if [ -z "$MDP" ]; then
   STOPPER_SYNC_MDP
   else
     if [ ! -e /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer ]; then
        sed -i "s/password: \"portnairAdmin\"/password: \"$MDP_E\"/g" /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js
        sed -i "s/password: \"portnairAdmin\"/password: \"$MDP_E\"/g" /var/lib/docker/volumes/ecombox_data/_data/main-es5.js
        #sed -i 's/password: \"portnairAdmin\"/password: \"$MDP\"/g' /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js.map
        else
           ANCIEN_MDP=$(cat /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer)
           # Échappement éventuel de l'ancien mdp
           ANCIEN_MDP_E=$(echo "$ANCIEN_MDP" | sed 's,\.\|\[\|\*\|\^\|\$\|/\|\\,\\&,g')
           sed -i "s/password: \"$ANCIEN_MDP_E\"/password: \"$MDP_E\"/g" /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js
           sed -i "s/password: \"$ANCIEN_MDP_E\"/password: \"$MDP_E\"/g" /var/lib/docker/volumes/ecombox_data/_data/main-es5.js
           #sed -i 's/password: \"$ANCIEN_MDP\"/password: \"$MDP\"/g' /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js.map
     fi
     echo -e "$MDP" > /var/lib/docker/volumes/ecombox_config/_data/.pwd_portainer
fi

echo -e "$COLINFO\n"
echo -e "La synchronisation du mot de passe \"admin\" de Portainer a été réalisée."
echo -e ""
echo -e "Vous devez vider votre cache et relancer le navigateur"
echo -e "$COLCMD"

exit 0
