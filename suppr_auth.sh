#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Suppression de l'authentification

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel du fichier de fonctions (qui fait lui-même appel au fichier de variable et à param.conf)
source $DOSSIER_INSTALL/fonctions.sh

echo -e "$COLDEFAUT"
echo -e "Suppression de l'authentification"
echo -e ""
echo -e "$COLINFO\c"
echo -e "Vous avez décidé de supprimer l'authentification pour accéder à l'interface d'e-comBox."

POURSUIVRE_SUPPR_AUTH

# Suppresion du fichier dans lequel se trouve les identifiants
rm -f /var/lib/docker/volumes/ecombox_config/_data/.auth

# Suppression de la configuration de nginx
sed -i '/auth_basic/d' /var/lib/docker/volumes/ecombox_conf_nginx/_data/default.conf

# Reload nginx
docker exec e-combox nginx -s reload

echo -e "$COLINFO\n"
echo -e "L'authentification pour accéder à l'interface d'e-comBox a été supprimée."
echo -e ""
echo -e "Vous pourrez relancez la procédure de configuration de l'authentification quand vous voulez en exécutant la commande suivante : bash /opt/e-comBox/ajout_auth.sh"
echo -e "$COLCMD"

exit 0
