# Version 4 de l'e-comBox sur Linux

**La version v4** :

- permet d'utiliser l'e-comBox derrière un Reverse Proxy externe (voir exemple de configuration ci-dessous), ce qui permet de ne dédier qu'une seule adresse IP publique pour l'ensemble des instances ;
- permet de s'affranchir des ports même s'il n'y a pas de Reverse Proxy externe (seul le port d'accès à l'application est exposé) ;
- intègre HTTPS ;
- améliore l'authentification (un professeur identifié sur l'interface ne gérera que ses propres sites) ;
- ne nécessite plus un client tel que Filezilla pour l'accès aux sources via le SFTP ;
- intègre les mentions légales ;
- permet de réinitialiser le mot de passe admin de Portainer (mot de passe qui doit obligatoirement être changé à l'installation.)

Il s'agit ici des scripts qui permettent d'installer l'application e-comBox sur un serveur Linux.
Ces scripts ont été testés sur les versions Linux (obligatoirement 64 bits) suivantes :

- Ubuntu (à partir la version 18.04) ;
- Debian 9 ;
- Debian 10,
- Debian 11 (actuellement stable).

## Configuration du Reverse Proxy - Exemple

> Note : la configuration d'un Reverse Proxy externe n'est utile que si vous avez plusieurs instances de l'ecomBox et que vous voulez les rendre accessible via une seule adresse IP ou un même nom de domaine.

**Adresse IP privée du serveur** sur lequel est installée e-comBox : 172.31.40.109

**Nom de domaine** : ecb.nom.domaine (cela fonctionne également avec une adresse IP publique)

**Chemin d'accès** : rne

On suppose que l'on conserve les ports par défaut à savoir (ces derniers sont totalement transparents pour l'utilisateur) :

- Port utilisé pour l'interface d'e-comBox (interne ou externe) ==> PORT_ECB=8888
- Port utilisé pour Portainer ==> PORT_PORTAINER=8880
- Port utilisé pour l'accès  aux sites ==> PORT_RP=8800
- Port pour le registry ==> PORT_REGISTRY=5443

**L'interface de l'e-comBox sera accessible** via l'URL : <https://ecb.nom.domaine/rne/app/>

**L'interface de Portainer sera accessible** via l'URL : <https://ecb.nom.domaine/rne/portainer/>

**Les sites seront accessibles** via un URL sous la forme : <https://ecb.nom.domaine/rne/nom_du_site/>

### Avec Nginx

Ci-dessous **le fichier default.conf** de Nginx :

```server {
  server_name ecb.nom.domaine;
  listen 80; # Éventuellement
  listen 443 ssl;

  # Chemin vers les certificats (le nom des fichiers certificats est libre)
  ssl_certificate ssl/ecb.nom.domaine.crt.pem;
  ssl_certificate_key ssl/ecb.nom.domaine.key.pem;

  ...

  location / {
    ...
    error_page 404 = @404;
    return 404;
    error_page 497 = @497;
    return 497;

  }

  ….
  # Pour Odoo sinon les CSS ne sont pas chargés
  location @404 {
     add_header Vary Referer;
     # À répéter pour chaque instance
     if ($http_referer ~ ://[^/]*(/rne/).*) {
     proxy_pass https://172.31.40.109:8800;
     }
  }
  
  # Pour Odoo avec le HTTPS sinon les CSS ne sont pas chargés
  location @497 {
     add_header Vary Referer;
     # À répéter pour chaque instance
     if ($http_referer ~ ://[^/]*(/rne/).*) {
     proxy_pass https://172.31.40.109:8800;
     }
  }
 
  location /rne/ {
     proxy_pass https://172.31.40.109:8800;
     proxy_redirect off;
  }

}
```

### Avec Apache

Installer le module "proxy_wstunnel" : a2enmod proxy_wstunnel

Ci-dessous **le fichier de configuration** d'Apache :

``` text
<VirtualHost *:443>
  ServerName ecb.nom.domaine

  ...
  
  # Gestion du mode SSL (le nom des fichiers certificats est libre)
  SSLEngine on
  SSLCertificateFile "ssl/ecb.nom.domaine.crt.pem"
  SSLCertificateKeyFile "ssl/ecb.nom.domaine.key.pem"
  ...
  
  ProxyPreserveHost On
  RewriteEngine On

  # Pour l'accès aux consoles des conteneurs via Portainer (ne pas oublier d’ajouter le module proxy_wstunnel)
  ProxyPassMatch "/rne/portainer/api/websocket/(.*)" wss://172.31.40.109:8880/api/websocket/$1

  # Pour l'accès à l'interface de l'instance
  ProxyPass /rne/ https://172.31.40.109:8800/rne/
  ProxyPassReverse /rne/ https://172.31.40.109:8800/rne/

  # Pour le chargement CSS des Odoo
  RewriteCond "%{HTTP_REFERER}" "://[^/]*(/rne/).*" [NC]
  RewriteRule ^(.*)$ https://172.31.40.109$1 [P]
  ...

  </VirtualHost>
```

## Installation de l'e-comBox

Il s'agit d'une installation classique. Le script permet de remplir les variables du fichier /opt/e-comBox/param.conf hormis les ports qu'il faut saisir à la main dans ce dernier fichier si on n'utilise pas les ports par défaut.

### **Si vous avez déjà une application e-comBox installée**

- Mettre la variable BRANCHE à "v4" dans /opt/e-comBox/configure_application.sh ==> BRANCHE=v4
- réinstaller l'application ==> bash /opt/e-comBox/configure_application.

> Note : la migration des sites est proposée. Si la conservation des sites ne vous intéresse pas, il vaut mieux tout supprimer via le script /opt/e-comBox/delete_conteneurs.sh ==> Après la suppression des conteneurs et des images, le script d’installation sera relancé automatiquement.

### **Si vous n'avez pas d'application e-comBox installée**

Après avoir pris les précautions d'usage si le serveur passe par un proxy (voir [ici](https://llb.ac-corse.fr/mw/index.php/Installation_sur_Linux_-_v3#Pr.C3.A9alables)) :

- wget https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/v4/install_linux_e-comBox.sh --output-document install_linux_e-comBox.sh
- bash install_linux_e-comBox.sh
- rm install_linux_e-comBox.sh

À noter que si vous avez déjà Docker et Docker-compose installés sur votre serveur, il suffit de récupérer et d'installer le script configure_application.sh :

- curl -fsSL https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/dev-https/configure_application.sh -o configure_application.sh
- bash configure_application.sh

**Si vous passez par un reverse proxy**, lors de la première installation de cette version, il faut faire attention à :

- mettre "O" à la question "L'accès à l'interface de l'e-comBox se fait-il via un reverse proxy (O/N))?" car le fichier param.conf est configuré pour une installation "classique" sans reverse proxy ;
- ne pas mettre de "/" dans le chemin.

Ci-dessous le **fichier /opt/e-comBox/param.conf** correctement rempli au regard de l'exemple fourni :

```bash
#!/bin/bash

# Définition des variables utiles à l'application

# Adresse IP privée
ADRESSE_IP_PRIVEE=172.31.40.109

# Adresse IP publique ou nom du domaine pleinement qualifié
DOMAINE=ecb.nom.domaine

# Utilisation d'un Reverse-Proxy - O/N (N par défaut) 
RV=O

# Chemin d'accès éventuel (en cas d'utilisation d'un Reverse-Proxy)
CHEMIN=rne 

# Port utilisé pour l'interface d'e-comBox (interne ou externe)
PORT_ECB=8888

# Port utilisé pour Portainer
PORT_PORTAINER=8880

# Port utilisé pour l'accès  aux sites
PORT_RP=8800

# Port utilisé pour le registry
PORT_REGISTRY=5443

# Adresse du Proxy
ADRESSE_PROXY=

# No Proxy
NO_PROXY=

# Les 4 variables qui suivent sont utiles pour créer un certificat auto signé
# Si vous avez déjà un certificat, saisir uniquement les valeurs des deux variables suivantes

# Code Pays sur 2 lettres
CODE_PAYS=FR

# Nom Pays
NOM_PAYS=France 

# Nom région
NOM_REGION="Corse"

# Nom organisation
NOM_ORGANISATION="ReseauCerta"

# Les 2 variables qui suivent sont utiles pour donner le chemin vers les éléments pour mettre en place un certificat existant

# Fichier certificat pour nginx existant avec le chemin complet /chemin/fichier.crt
CHEMIN_CERT=

# Fichier de la clé privée pour nginx existante avec le chemin complet /chemin/fichier.key
CHEMIN_KEY=

```

> Note
>
> En ce qui concerne les certificats, si l’on passe par un reverse proxy, les certificats de ce dernier sont propagés et il n’est théoriquement pas besoin de les installer sur les instances (vous pouvez donc laisser le certificat se créer automatiquement comme proposé dans le script).

## Gestion de l'authentification

L'authentification se réalise maintenant via Portainer. L'administrateur doit se connecter sur Portainer et créer des utilisateurs dans le groupe "Prof" automatiquement créé à l'installation.

Le professeur peut ensuite modifier son mot de passe via l'application.

Les professeurs ne voient et ne peuvent agir que sur les sites qu'ils ont eux-même créés.

> **Note** : le compte "admin" de Portainer peut se connecter sur l'e-comBox et créer des sites mais cela n'est pas conseillé.
