#!/bin/bash
# shellcheck disable=SC2034

# Définition de la branche, de la version des applications et des URL vers les dépôts
BRANCHE=v4
VERSION_APPLI=4.0
TAG=4.0
DEPOT_GITLAB_SCRIPTS="https://gitlab.com/e-combox/e-comBox_scriptsLinux"
DEPOT_GITLAB_PORTAINER="https://gitlab.com/e-combox/e-comBox_portainer"
DEPOT_GITLAB_RV="https://gitlab.com/e-combox/e-comBox_reverseproxy"
DEPOT_GITLAB_DC="https://gitlab.com/e-combox/e-comBox_docker-compose"

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Définition du dossier de migration
DOSSIER_MIGRATION="$DOSSIER_INSTALL/migration"

# Définition du dossier des certificats
DOSSIER_CERTS="$DOSSIER_INSTALL/certs"

# Couleurs
COLTITRE="\033[1;35m"  # Rose
COLPARTIE="\033[1;34m" # Bleu
COLTXT="\033[0;37m"    # Gris
COLCHOIX="\033[1;33m"  # Jaune
COLDEFAUT="\033[0;33m" # Brun-jaune
COLSAISIE="\033[1;32m" # Vert
COLCMD="\033[1;37m"    # Blanc
COLSTOP="\033[1;31m"   # Rouge
COLINFO="\033[0;36m"   # Cyan

