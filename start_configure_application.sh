#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox
DEPOT_GITLAB_SCRIPTS='https://gitlab.com/e-combox/e-comBox_scriptsLinux'
BRANCHE=v4

initInstall() {
   # Téléchargement de la licence CeCILL
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/LICENCE -o LICENCE
   echo -e ""
   echo -e "$COLINFO"
   echo -e "Vous devez lire et accepter les termes de la licence CeCILL avant d'installer (ou de mettre à jour) et de pouvoir utiliser e-comBox $VERSION_APPLI."
   echo -e "$COLCMD"
   echo -e "Appuyer sur n'importe quelle touche pour lire la licence CeCILL."
   echo -e ""
   if [ ! -e LICENCE ]; then
      echo -e "$COLINFO"
      echo -e "Le fichier contenant les termes de la licence n'a pas pu être téléchargé."
      echo -e "Vérifiez la configuration de votre proxy pour l'outil curl."
      echo -e "Éventuellement, téléchargez le fichier par vos propres moyens via l'URL suivante https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/$BRANCHE/LICENCE."
      echo -e ""
      echo -e "Relancez-ensuite le script : bash configure_application.sh."
      echo -e ""
      echo -e "Appuyer sur n'importe quelle touche pour arrêter le script."
      read -r _ENTREE
   else
      read -r _CONTENU
      more LICENCE
      echo -e "Acceptez-vous les termes de la licence CeCCILL (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
      echo -e ""
      read -r SAISIE
      if [ "$SAISIE" = n ]; then
         echo -e "$COLSTOP"
         echo -e "Vous avez décidé de ne pas continuer à installer et configurer e-comBox. Vous pouvez reprendre la procédure quand vous voulez."
         echo -e "$COLTXT"
         exit 1
      else
         # Création ou réinitialisation des fichiers de log
         echo -e "$COLTITRE\n"
         echo -e "Création des fichiers de log : /var/log/ecombox.log et /var/log/ecombox-error.log"
         echo -e "Configuration d'e-comBox le $(date)\n" >/var/log/ecombox.log
         echo -e "Configuration d'e-comBox le $(date)\n" >/var/log/ecombox-error.log

         echo -e "$COLCMD"

         #Création du dossier d'installation de l'application s'il n'existe pas déjà
         if [ ! -d "$DOSSIER_INSTALL" ]; then
            mkdir $DOSSIER_INSTALL
         fi

         # Bascule du fichier licence vers $DOSSIER_INSTALL
         mv LICENCE $DOSSIER_INSTALL/ 2>>/var/log/ecombox.log

         # Téléchargement du fichier contenant les paramètres s'il n'existe pas déjà
         if [ ! -e "$DOSSIER_INSTALL/param.conf" ]; then
            curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/param.conf -o $DOSSIER_INSTALL/param.conf
         fi

         # Téléchargement du fichier des variables
         curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/variables.sh -o $DOSSIER_INSTALL/variables.sh

         # Récupération sur gitlab du fichier de fonctions
         curl -fsSL https://gitlab.com/e-combox/e-comBox_scriptsLinux/raw/$BRANCHE/fonctions.sh -o $DOSSIER_INSTALL/fonctions.sh

         # Appel des fichier de paramètres, variables et fonctions
         source $DOSSIER_INSTALL/param.conf
         source $DOSSIER_INSTALL/variables.sh
         source $DOSSIER_INSTALL/fonctions.sh
         #source ./fonctions.sh

         # Vérification de la version installée
         verifVersion
      fi
   fi
}

installer() {
   clear
   echo -e "$COLTITRE"
   echo "**********************************************************"
   echo "*  INSTALLATION DE E-COMBOX V4 ET CONFIGURATION   *"
   echo "*                DE SON ENVIRONNEMENT                    *"
   echo "**********************************************************"

   # Test des variables relatives à l'adressage IP
   test_adresse_ip_privee=$(cat $DOSSIER_INSTALL/param.conf | grep ADRESSE_IP_PRIVEE)

   if [ -z "$test_adresse_ip_privee" ]; then
      # Ça veut dire qu'il s'agit de l'ancien fichier, il faut remplacer la variable "ADRESSE_IP"
      # Adresse IP privée
      sed -i "s/ADRESSE_IP=/ADRESSE_IP_PRIVEE=/g" $DOSSIER_INSTALL/param.conf
      ADRESSE_IP_PRIVEE=$ADRESSE_IP

      # On remplace également le commentaire de la ligne
      sed -i "s/# Adresse IP ou nom */# Adresse IP privée/g" $DOSSIER_INSTALL/param.conf
   fi

   # On refait un test au cas où il n'y avait plus la variable d'adressage IP
   test_adresse_ip_privee=$(cat $DOSSIER_INSTALL/param.conf | grep ADRESSE_IP_PRIVEE)

   if [ -z "$test_adresse_ip_privee" ]; then
      echo -e "\n# Adresse IP privée\nADRESSE_IP_PRIVEE=127.0.0.1" >>$DOSSIER_INSTALL/param.conf
      ADRESSE_IP_PRIVEE=127.0.0.1
   fi

   test_domaine=$(cat $DOSSIER_INSTALL/param.conf | grep DOMAINE)

   if [ -z "$test_domaine" ]; then
      sed -ir "/^ADRESSE_IP_PRIVEE/a# Adresse IP publique ou nom du domaine pleinement qualifié\nDOMAINE=" $DOSSIER_INSTALL/param.conf
   fi

   #Gestion de l'adresse IP
   echo -e "$COLPARTIE"
   echo -e "Configuration du ou des adresses IP et éventuellement du nom de domaine."

   # Adresse IP privée
   echo -e "$COLTXT"
   echo -e "L'adresse IP privée est actuellement configurée à ${COLCHOIX}$ADRESSE_IP_PRIVEE${COLTXT}"
   echo -e "Vous pouvez valider ce choix ou bien saisir une autre valeur.${COLINFO}\n"
   echo -e "ATTENTION, l'adresse IP privée doit obligatoirement être configurée même si un nom de domaine ou une adresse IP publique sera ajoutée pour que les sites soient accessibles de l'extérieur.${COLSAISIE}\n"
   echo -e "Validez l'adresse IP privée proposée sinon saisissez une autre valeur : ${COLCHOIX}$ADRESSE_IP_PRIVEE"
   read -r ADRESSE_IP_PRIVEE_SAISIE

   if [ -z "$ADRESSE_IP_PRIVEE_SAISIE" ]; then
      echo -e "$COLINFO"
      echo -e "Vous avez maintenu la valeur ${COLCHOIX}$ADRESSE_IP_PRIVEE"
   else
      echo -e "$COLINFO"
      echo -e "La valeur saisie va être mise à jour dans le fichier de configuration $DOSSIER_INSTALL/param.conf"
      sed -i "s/ADRESSE_IP_PRIVEE=$ADRESSE_IP_PRIVEE/ADRESSE_IP_PRIVEE=$ADRESSE_IP_PRIVEE_SAISIE/g" $DOSSIER_INSTALL/param.conf
      ADRESSE_IP_PRIVEE=$ADRESSE_IP_PRIVEE_SAISIE
   fi

   # Adresse IP publique
   echo -e "$COLTXT"
   if [ -z "$DOMAINE" ]; then
      echo -e "Aucun nom de domaine pleinement qualifié (ou adresse IP publique) n'est actuellement configurée. Vous pouvez valider ce choix ou bien saisir une autre valeur.${COLINFO}\n"
   else
      echo -e "Le nom de domaine pleinement qualifié (ou l'adresse IP publique) qui sera utilisée pour chaque site créé est actuellement configurée à ${COLCHOIX}$DOMAINE${COLTXT}. Vous pouvez valider ce choix ou bien saisir une autre valeur.${COLINFO}\n"
   fi
   echo -e "ATTENTION, c'est ce nom de domaine ou cette adresse IP qui sera utilisée à la place de l'adresse IP privée. Aucune valeur ne doit donc être saisie ici si vous ne voulez pas cela, notamment si le serveur ne doit pas être accessible de l'extérieur.${COLSAISIE}\n"
   echo -e "Validez l'adresse IP publique (ou le nom de domaine) proposée sinon saisissez une autre valeur : ${COLCHOIX}$DOMAINE"
   read -r DOMAINE_SAISIE

   if [ -z "$DOMAINE_SAISIE" ]; then
      if [ -z "$DOMAINE" ]; then
         echo -e "$COLINFO"
         echo -e "Aucune adresse IP publique ou nom de dommaine n'a été configuré."
      else
         echo -e "$COLINFO"
         echo -e "Vous avez maintenu la valeur ${COLCHOIX}$DOMAINE."
      fi
   else
      echo -e "$COLINFO"
      echo -e "La valeur saisie va être mise à jour dans le fichier de configuration $DOSSIER_INSTALL/param.conf"
      sed -i "s/DOMAINE=$DOMAINE/DOMAINE=$DOMAINE_SAISIE/g" $DOSSIER_INSTALL/param.conf
      DOMAINE=$DOMAINE_SAISIE
   fi

   # Gestion des variables RV et CHEMIN pour un passage éventuel par un reverse proxy

   test_rv=$(cat $DOSSIER_INSTALL/param.conf | grep RV)

   # On fixe la variable RV à un "0" en majuscule
   if [ "$RV" = "o" ]; then
      RV=O
      sed -i "s/RV=o/RV=$RV/g" $DOSSIER_INSTALL/param.conf
   fi

   if [ -z "$test_rv" ]; then
      sed -ir "/^DOMAINE/a# Utilisation d'un Reverse-Proxy - O/N (N par défaut)\nRV=N" $DOSSIER_INSTALL/param.conf
      RV=N
   fi

   test_chemin=$(cat $DOSSIER_INSTALL/param.conf | grep CHEMIN)

   if [ -z "$test_chemin" ]; then
      sed -ir "/^RV=/a# Chemin d'accès éventuel (en cas d'utilisation d'un Reverse-Proxy)\nCHEMIN=" $DOSSIER_INSTALL/param.conf
   fi

   echo -e "$COLTXT"
   echo -e "Merci de répondre aux questions suivantes qui permettent de savoir si vous passez par un reverse proxy avec l'introduction d'un chemin d'accès dans l'URL."
   echo -e ""
   echo -e "L'accès à l'interface de l'e-comBox se fait-il via un reverse proxy (${COLCHOIX}O/N${COLTXT}) ?. Validez pour maintenir la valeur actuelle qui est ${COLCHOIX}$RV${COLTXT} ou saisissez une autre valeur : ${COLSAISIE}\c"
   read -r RV_SAISI

   if [ -z "$RV_SAISI" ]; then
      echo -e "$COLINFO"
      echo -e "Vous avez maintenu la valeur ${COLCHOIX}$RV"
   else
      echo -e "$COLINFO"
      echo -e "La valeur saisie va être mise à jour dans le fichier de configuration $DOSSIER_INSTALL/param.conf"
      if [ "$RV_SAISI" = "o" ]; then
         RV_SAISI="O"
      fi
      sed -i "s/RV=$RV/RV=$RV_SAISI/g" $DOSSIER_INSTALL/param.conf
      RV=$RV_SAISI
   fi

   if [ "$RV" = "O" ]; then
      echo -e "$COLTXT"
      echo -e "Saisissez le chemin d'accès (sans \"/\"). Validez pour maintenir la valeur actuelle ou saisissez une autre valeur ${COLCHOIX}$CHEMIN${COLSAISIE}: \c"
      read -r CHEMIN_SAISI
      if [ -z "$CHEMIN_SAISI" ]; then
         echo -e "$COLINFO"
         echo -e "Vous avez maintenu la valeur ${COLCHOIX}$CHEMIN"
      else
         echo -e "$COLINFO"
         echo -e "La valeur saisie va être mise à jour dans le fichier de configuration $DOSSIER_INSTALL/param.conf"
         sed -i "s/CHEMIN=$CHEMIN/CHEMIN=$CHEMIN_SAISI/g" $DOSSIER_INSTALL/param.conf
         CHEMIN=$CHEMIN_SAISI
      fi
      echo -e "$COLTXT"
      echo -e "Dans le cas d'utilisation d'un reverse proxy, il est quand même nécessaire de définir, pour chaque instance, des ports d'écoute."
   fi

   # Gestion des ports
   echo -e "$COLPARTIE"
   echo -e "Configuration des ports nécessaires"

   test_port_rp=$(cat $DOSSIER_INSTALL/param.conf | grep PORT_RP)

   if [ -z "$test_port_rp" ]; then
      echo -e "\n# Port utilisé pour l'accès  aux sites\nPORT_RP=8800" >>$DOSSIER_INSTALL/param.conf
      PORT_RP=8800
   fi

   test_port_registry=$(cat $DOSSIER_INSTALL/param.conf | grep PORT_REGISTRY)

   if [ -z "$test_port_registry" ]; then
      sed -ir "/^PORT_RP/a# Port utilisé pour le registry\nPORT_REGISTRY=5443" $DOSSIER_INSTALL/param.conf
      PORT_REGISTRY=5443
   fi

   # À terme permettre de le modifier via le script et de modifier le message en cas de passage par un rv
   echo -e "$COLINFO"
   echo -e "Le port pour l'accès à l'application et aux sites est le suivant : ${COLCHOIX}$PORT_RP"
   echo -e "$COLINFO"

   echo -e "Les autres ports qui seront utilisés par défaut sur les serveurs (mais qu'il est inutile de rediriger) sont les suivants :"
   echo -e ""
   echo -e "Port pour le lancement de l'application : ${COLCHOIX}$PORT_ECB"
   echo -e "$COLINFO"
   echo -e "Port pour le lancement de Portainer : ${COLCHOIX}$PORT_PORTAINER"
   echo -e "$COLINFO"
   echo -e "Port pour le lancement du registry : ${COLCHOIX}$PORT_REGISTRY"
   echo -e ""
   echo -e "$COLSAISIE"
   echo -e "Vous pouvez modifier ces valeurs en éditant le fichier $DOSSIER_INSTALL/param.conf puis en relançant ce script."

   POURSUIVRE

   apt update
   apt install -y jq

   #Gestion du proxy
   echo -e "$COLPARTIE"
   echo -e "Configuration éventuelle du proxy"

   test_proxy=$(cat $DOSSIER_INSTALL/param.conf | grep -v '#' | grep ADRESSE_PROXY)

   if [ -z "$test_proxy" ]; then
      echo -e "\n# Adresse du Proxy\nADRESSE_PROXY=$http_proxy" >>$DOSSIER_INSTALL/param.conf
      ADRESSE_PROXY=$http_proxy
   fi

   test_no_proxy=$(cat $DOSSIER_INSTALL/param.conf | grep -v '#' | grep NO_PROXY)

   if [ -z "$test_no_proxy" ]; then
      echo -e "\n# No proxy\nNO_PROXY=" >>$DOSSIER_INSTALL/param.conf
      NO_PROXY=""
   fi

   if [ -z "$ADRESSE_PROXY" ]; then
      echo -e "$COLINFO"
      echo -e "Vous n'avez pas configuré de proxy pour e-comBox.${COLTXT}"
      echo -e "Confirmez-vous ce choix (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
      read -r REPONSE
      if [ -z "$REPONSE" ]; then
         REPONSE="o"
      fi
   else
      echo -e "$COLINFO"
      echo -e "L'adresse de proxy actuellement configurée est ${COLCHOIX}${ADRESSE_PROXY}${COLTXT}."
      echo -e "Confirmez-vous cette adresse de proxy pour vous connecter à internet (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
      read -r REPONSE
      if [ -z "$REPONSE" ]; then
         REPONSE="o"
      fi
   fi

   if [ "$REPONSE" != "o" ]; then
      echo -e "$COLTXT"
      echo -e "Saisissez l'adresse du proxy : [${COLDEFAUT}${http_proxy}${COLTXT}] ${COLSAISIE}\c"
      echo -e "(Validez l'adresse du proxy proposée sinon saisir ip-proxy:port) :"
      read -r ADRESSE_PROXY_SAISIE
      if [ -z "$ADRESSE_PROXY_SAISIE" ]; then
         ADRESSE_PROXY_SAISIE="$http_proxy"
      else
         export http_proxy="$ADRESSE_PROXY_SAISIE"
         export https_proxy="$ADRESSE_PROXY_SAISIE"
      fi
      #sed -e '/^#/d' -i "s/.*ADRESSE_PROXY=.*/ADRESSE_PROXY=$ADRESSE_PROXY_SAISIE/g" $DOSSIER_INSTALL/param.conf
      sed -i "s/ADRESSE_PROXY=.*/ADRESSE_PROXY=$ADRESSE_PROXY_SAISIE/g" $DOSSIER_INSTALL/param.conf
      echo -e "$COLTXT"
      echo -e "Saisissez les hôtes à ignorer par le proxy : [${COLDEFAUT}${NO_PROXY}${COLTXT}] ${COLSAISIE}\c"
      echo -e "(Validez les hôtes à ignorer ou saisissez les hôtes séparés par une virgule sans espace (les caractères spéciaux comme \".\" ou \"*\" sont acceptés). Pour éventuellement remettre à blanc le contenu, saisissez ${COLDEFAUT}\"n\"${COLTXT} :"
      read -r NO_PROXY_SAISI
      if [ -n "$NO_PROXY_SAISI" ] && [ "$NO_PROXY_SAISI" != "n" ]; then
         #sed -e '/^#/d' -i "s/.*NO_PROXY=.*/NO_PROXY=$NO_PROXY_SAISI/g" $DOSSIER_INSTALL/param.conf
         sed -i "s/NO_PROXY=.*/NO_PROXY=$NO_PROXY_SAISI/g" $DOSSIER_INSTALL/param.conf
      elif [ "$NO_PROXY_SAISI" = "n" ]; then
         #sed -e '/^#/d' -i "s/.*NO_PROXY=.*/NO_PROXY=/g" $DOSSIER_INSTALL/param.conf
         sed -i "s/.*NO_PROXY=.*/NO_PROXY=/g" $DOSSIER_INSTALL/param.conf
      fi
   fi

   # Recharge du fichier de paramètres
   source $DOSSIER_INSTALL/param.conf

   # Affichage des informations
   echo -e "$COLTXT"
   echo -e "Vous vous apprêtez à utiliser les paramètres suivants:"
   echo -e "$COLINFO"
   echo -e "  - Adresse IP privée : ${COLCHOIX}$ADRESSE_IP_PRIVEE${COLINFO}"
   echo -e "  - Adresse IP publique ou nom de domaine : ${COLCHOIX}$DOMAINE${COLINFO}"
   echo -e "  - Passage par un reverse proxy : ${COLCHOIX}$RV${COLINFO}"
   echo -e "  - Chemin : ${COLCHOIX}$CHEMIN${COLINFO}"
   echo -e "  - Port e-combox : ${COLCHOIX}$PORT_ECB${COLINFO}"
   echo -e "  - Port pour l'accessibilité de l'interface et des sites : ${COLCHOIX}$PORT_RP${COLINFO}"
   echo -e "  - Port Portainer : ${COLCHOIX}$PORT_PORTAINER${COLINFO}"
   echo -e "  - Port Registry : ${COLCHOIX}$PORT_REGISTRY${COLINFO}"
   echo -e "  - Proxy : ${COLCHOIX}$ADRESSE_PROXY${COLINFO}"
   echo -e "  - No Proxy : ${COLCHOIX}$NO_PROXY${COLCMD}"

   {
      echo -e "\nL'adresse IP privée utilisée est $ADRESSE_IP_PRIVEE\n"
      echo -e "L'adresse IP publique ou le nom de domaine utilisé est $DOMAINE\n"
      echo -e "Passage par un RV : $RV\n"
      echo -e "Chemin : $CHEMIN\n"
   } >>/var/log/ecombox.log

   POURSUIVRE

   # Création du fichier config.json s'il n'existe pas
   if [ ! -e ~/.docker ]; then
      mkdir ~/.docker
      echo -e "$COLDEFAUT"
   fi

   if [ ! -e ~/.docker/config.json ]; then
      echo -e "Création d'un fichier vide config.json"
      echo -e "$COLCMD\c"
      echo "" >~/.docker/config.json
      chown -R "$USER":docker ~/.docker
      chmod g+rw ~/.docker/config.json
   fi

   #Configuration du proxy pour GIT et pour Docker

   if [ "$ADRESSE_PROXY" != "" ]; then
      echo -e "$COLDEFAUT"
      echo -e "Congiguration de GIT pour le proxy"
      echo -e "$COLCMD\c"
      git config --global http.proxy "$ADRESSE_PROXY"
      if [ ! -d "/etc/systemd/system/docker.service.d" ]; then
         mkdir /etc/systemd/system/docker.service.d
      fi
      echo -e "$COLDEFAUT"
      echo "Ajout des variables d'environnement à systemd (/etc/systemd/system/docker.service.d/http-proxy.conf)"
      echo -e "$COLCMD\c"
      {
         echo "[Service]"
         echo "Environment=\"HTTP_PROXY=http://$ADRESSE_PROXY\""
         echo "Environment=\"HTTPS_PROXY=http://$ADRESSE_PROXY\""
         echo "Environment=\"NO_PROXY=$NO_PROXY\""
      } >/etc/systemd/system/docker.service.d/http-proxy.conf
      echo ""
      echo -e "Redémarrage de Docker"
      systemctl daemon-reload
      systemctl restart docker
      echo -e "$COLDEFAUT"
      echo -e "Ajout des paramètres du Proxy dans ~/.docker/config.json après sauvegarde du fichier d'origine et du fichier actuel."
      echo -e "$COLCMD\c"
      echo "Ajout des paramètres du proxy $ADRESSE_PROXY et $NO_PROXY" >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log

      # Sauvegarde du fichier config.json d'origine si ce n'est déjà fait
      if [ ! -e ~/.docker/config.json.ori ]; then
         cp ~/.docker/config.json ~/.docker/config.json.ori
      fi
      # Sauvegarde du fichier actuel et restauration du fichier d'origine
      cp ~/.docker/config.json ~/.docker/config.json.sauv
      cp ~/.docker/config.json.ori ~/.docker/config.json
      #echo "" > ~/.docker/config.json
      #chown -R $USER:docker ~/.docker
      #chmod g+rw ~/.docker/config.json

      # Ajout des paramètres du proxy au fichier config.json
      if [ "$(sed -n '$=' /root/.docker/config.json)" = 1 ] || [ -z "$(sed -n '$=' /root/.docker/config.json)" ]; then
         echo "le fichier config.json est bien vide." >>/var/log/ecombox.log
         sed -i "1i \ {\n\t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
      else
         echo "le fichier config.json n'est pas vide" >>/var/log/ecombox.log
         sed -i "/\"proxies\": {/,10d" ~/.docker/config.json
         if [ "$(sed -n '$=' /root/.docker/config.json)" = 2 ]; then
            echo "le fichier config.json a 2 lignes." >>/var/log/ecombox.log
            sed -i "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
         else
            echo "Le fichier config.json a plus de 2 lignes." >>/var/log/ecombox.log
            sed -i "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n\t  }\n\t},\n" ~/.docker/config.json
         fi
      fi
   else
      echo -e "$COLINFO"
      echo -e "Aucun proxy configuré sur le système."
      echo -e "Les paramètres du proxy, s'ils existent, sont supprimés."
      echo -e "$COLCMD"
      echo "Aucun proxy configuré sur le système" >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
      git config --global --unset http.proxy
      if [ -f "/etc/systemd/system/docker.service.d/http-proxy.conf" ]; then
         rm -f /etc/systemd/system/docker.service.d/http-proxy.conf
         systemctl daemon-reload
         systemctl restart docker
      fi
      # Sauvegarde du fichier actuel
      cp ~/.docker/config.json ~/.docker/config.json.sauv
      # Suppression des paramètres du proxy du fichier config.json
      echo "Suppression des paramètres du proxy du fichier config.json"
      sed -i "/\"proxies\": {/,9d" ~/.docker/config.json
      if [ "$(sed -n '$=' /root/.docker/config.json)" -le 3 ]; then
         echo "Le fichier config.json est supprimé"
         rm -rf ~/.docker/config.json
      fi
   fi

   # Création du réseau pour l'application

   echo -e "$COLPARTIE"
   echo -e "Création ou modification du réseau pour e-comBox"
   echo -e "$COLCMD"

   if (docker network ls | grep bridge_e-combox); then
      NET_ECB=$(docker network inspect --format='{{range .IPAM.Config}}{{.Subnet}}{{end}}' bridge_e-combox)
      echo -e "$COLINFO"
      echo -e "Le système constate que le réseau ${COLCHOIX}${NET_ECB}${COLINFO} est déjà créé."
      echo -e "Si vous désirez modifier les paramètres de ce réseau, les sites existants seront supprimés."
      echo -e ""
      echo -e "${COLTXT}Voulez-vous modifier le réseau ? $COLSAISIE\c"
      echo -e "(tapez oui pour modifier le réseau et SUPPRIMER les sites ou sur n'importe quel autre touche pour continuer)."
      read -r CONFIRM_RESEAU
      if [ "$CONFIRM_RESEAU" = "oui" ]; then
         docker rm -f "$(docker ps -aq)"
         docker volume rm "$(docker volume ls -qf dangling=true)"
         docker network rm bridge_e-combox
         echo -e "$COLTXT\n"
         echo "Saisissez le nouveau réseau sous la forme ${COLCHOIX}adresseIP/CIDR${COLSAISIE}."
         read -r NET_ECB
         echo ""
         echo "$COLCMD"
         docker network create --subnet "$NET_ECB" bridge_e-combox 2>>/var/log/ecombox-error.log
         echo -e "$COLINFO"
         echo -e "Le nouveau réseau ${COLCHOIX}$NET_ECB${COLINFO} a été créé."
      else
         echo -e "$COLTXT"
         echo -e "Vous avez décidé de ne pas modifier le réseau."
      fi
   else
      echo -e "$COLINFO"
      echo -e ""
      echo -e "Le réseau d'e-comBox sera défini par défaut à ${COLCHOIX}192.168.97.0/24${COLTXT}."
      echo -e "Voulez-vous changer ce paramétrage ? $COLSAISIE\c"
      echo -e "(tapez oui pour changer l'adresse IP du réseau créé par défaut ou sur n'importe quelle touche pour continuer sans changement)."
      read -r CONFIRM_RESEAU
      if [ "$CONFIRM_RESEAU" = "oui" ]; then
         echo -e "$COLSAISIE\n"
         echo "Saisissez l'adresse du réseau sous la forme \"adresseIP/CIDR\"."
         read -r NET_ECB
         echo -e ""
         echo -e "$COLCMD"
         docker network create --subnet "$NET_ECB" bridge_e-combox 2>>/var/log/ecombox-error.log
         echo -e "$COLINFO"
         echo -e "Le réseau $NET_ECB a été créé."
      else
         NET_ECB="192.168.97.0/24"
         echo -e ""
         echo -e "$COLCMD"
         docker network create --subnet $NET_ECB bridge_e-combox 2>>/var/log/ecombox-error.log
         echo -e "$COLINFO"
         echo -e "Le réseau ${COLCHOIX}$NET_ECB${COLINFO} a été créé."
      fi
   fi

   echo -e "Le réseau ${COLCHOIX}$NET_ECB${COLINFO} est utilisé." >>/var/log/ecombox.log
   echo "" >>/var/log/ecombox.log

   # Reverse Proxy Nginx

   #Récupération du reverse proxy
   echo -e "$COLPARTIE"
   echo -e "Récupération et configuration du reverse proxy"
   echo -e "$COLCMD"

   if [ -d "$DOSSIER_INSTALL/e-comBox_reverseproxy" ]; then
      echo -e "$COLTXT"
      echo "Le reverse proxy existe et va être remplacé."
      echo -e "$COLCMD\c"
      echo -e "Le reverse proxy existe et va être remplacé." >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log

      cd $DOSSIER_INSTALL/e-comBox_reverseproxy || exit
      docker-compose down

      # Suppression des volumes (sauf celui contenant les certificats)
      docker volume rm e-combox_reverseproxy_nginx-html >>/var/log/ecombox-error.log
      docker volume rm e-combox_reverseproxy_nginx-docker-gen-templates >>/var/log/ecombox-error.log

      # Récupération éventuelle des nouvelles images
      docker pull reseaucerta/nginx:"$TAG"
      docker pull reseaucerta/docker-gen:"$TAG"

      cd ..
      rm -rf $DOSSIER_INSTALL/e-comBox_reverseproxy
   fi

   cd $DOSSIER_INSTALL || exit
   git clone -b $BRANCHE "$DEPOT_GITLAB_RV".git 2>>/var/log/ecombox-error.log

   #Configuration de l'adresse IP
   echo -e "$COLINFO"
   echo "Mise à jour de $DOSSIER_INSTALL/e-comBox_reverseproxy/.env."
   echo -e "$COLCMD"

   # Ajout des variables d'environnement à Nginx
   NGINX_HOST="$ADRESSE_IP_PRIVEE"
   if [ "$RV" = "O" ]; then
      if [ -n "$DOMAINE" ]; then
         FQDN="$DOMAINE/$CHEMIN"
      else
         FQDN="$ADRESSE_IP_PRIVEE/$CHEMIN"
      fi
   else
      if [ -n "$DOMAINE" ]; then
         FQDN="$DOMAINE:$PORT_RP"
         NGINX_HOST="$DOMAINE"
      else
         FQDN="$ADRESSE_IP_PRIVEE:$PORT_RP"
      fi
   fi
   {
      echo -e "ADRESSE_IP_PRIVEE=$ADRESSE_IP_PRIVEE"
      echo -e "DOMAINE=$DOMAINE"
      echo -e "NGINX_HOST=$NGINX_HOST"
      echo -e "NGINX_PORT=$PORT_RP"
      echo -e "FQDN=$FQDN"
      echo -e "PORT_REGISTRY=$PORT_REGISTRY"

      # Information si le reverse proxy est utilisé ou non (s'il est utilisé, il faut prendre la variable FQDN pour les liens vers les sites et démarrer les conteneurs sur l'adresse privée)
      echo -e "RV=$RV"

   } >$DOSSIER_INSTALL/e-comBox_reverseproxy/.env

   #Ajout des variables CHEMIN et CHEMIN_ABSOLU
   #On a besoin du chemin absolu dans le docker compose du sftp et pour le index.html de l'application
   echo -e "CHEMIN=$CHEMIN" >>$DOSSIER_INSTALL/e-comBox_reverseproxy/.env
   if [ -n "$CHEMIN" ]; then
      CHEMIN_ABSOLU=/$CHEMIN
      echo -e "CHEMIN_ABSOLU=/$CHEMIN" >>$DOSSIER_INSTALL/e-comBox_reverseproxy/.env
   else
      CHEMIN_ABSOLU=$CHEMIN
      echo -e "CHEMIN_ABSOLU=$CHEMIN" >>$DOSSIER_INSTALL/e-comBox_reverseproxy/.env
   fi

   echo ""

   # Lancement du reverse proxy
   echo -e "$COLPARTIE"
   echo -e "Lancement du reverse proxy"
   echo -e "$COLCMD\c"

   cd $DOSSIER_INSTALL/e-comBox_reverseproxy/ || exit
   docker-compose up -d 2>>/var/log/ecombox-error.log

   if [ $? = 0 ]; then
      echo -e "$COLINFO"
      echo -e "Le reverse proxy a été lancé.\n"
      echo -e "$COLCMD\n"
      echo -e "Le reverse proxy a été lancé." >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
   else
      echo -e "$COLINFO"
      echo -e "Le reverse proxy n'a pas pu être lancé. Consultez les log d'erreur /var/log/ecombox-error.log.\n"
      echo -e "$COLCMD"
      echo -e "Le reverse proxy n'a pas pu être lancé. Consultez les log d'erreur /var/log/ecombox-error.log." >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
   fi

   # Création ou mise à jour du certificat

   echo -e "$COLPARTIE"
   echo -e "Gestion du certificat pour le reverse proxy "
   echo -e "$COLCMD\c"

   if [ -e "$DOSSIER_CERTS/ecombox.crt" ] && [ -e "$DOSSIER_CERTS/ecombox.key" ]; then
      echo -e "$COLINFO"
      echo -e "Le système constate que vous disposez déjà un certificat. Voulez-vous l'utilisez (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c"
      read -r REPONSE_UPDATE
      if [ -z "$REPONSE_UPDATE" ]; then
         REPONSE_UPDATE="o"
      fi
   else
      # Si les fichiers relatifs au certificat n'existent pas on doit les créer
      CREATE_CERTIFICAT
   fi

   if [ "$REPONSE_UPDATE" = "o" ]; then
      cp "$DOSSIER_CERTS"/ecombox.crt /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.crt
      cp "$DOSSIER_CERTS"/ecombox.key /var/lib/docker/volumes/e-combox_reverseproxy_nginx-ssl/_data/ecombox.key
      docker exec nginx nginx -s reload
   fi

   # Registry

   #Récupération du registry
   echo -e "$COLPARTIE"
   echo -e "Lancement du registry"
   echo -e "$COLCMD"

   #port=$(docker port e-combox_registry | cut -d ":" -f2)

   #if docker ps | grep e-combox_registry && docker port e-combox_registry 5000 | grep "$PORT_REGISTRY"; then
   if docker ps | grep e-combox_registry | grep "$PORT_REGISTRY"; then
      echo -e "$COLINFO"
      echo -e "Le registry existe.\n"
      echo -e "$COLCMD"
   else
      echo -e "$COLINFO"
      echo -e "Il n'y a pas de registry lancé avec le bon port.\n"
      echo -e "$COLCMD"
      if docker ps -a | grep e-combox_registry || ! docker port e-combox_registry 5000 | grep "$PORT_REGISTRY"; then
         docker stop e-combox_registry 2>>/var/log/ecombox-error.log
         docker rm e-combox_registry 2>>/var/log/ecombox-error.log
         echo -e "$COLINFO"
         echo -e "Le registry existe mais n'est pas correctement lancé, il sera supprimé puis recréé." >>/var/log/ecombox-error.log
         echo -e "Le registry existe mais n'est pas correctement lancé, il sera supprimé puis recréé.\n"
         echo -e "$COLCMD"
      fi

      # Création des certificats du registry

      if [ ! -d "$DOSSIER_CERTS/registry" ]; then
         mkdir -p $DOSSIER_INSTALL/certs/registry
      fi

      # Certificat pour le push
      openssl req -new -subj "/C=$CODE_PAYS/ST=$NOM_PAYS/L=$NOM_REGION/O=$NOM_ORGANISATION/CN=localhost" -addext "subjectAltName = DNS:localhost" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -keyout "$DOSSIER_CERTS"/registry/localhost.key -out "$DOSSIER_CERTS"/registry/localhost.crt

      # Certificat pour récupérer les images
      if [ -n "$CHEMIN" ]; then
         DNS=$DOMAINE
      else
         if [ -n "$DOMAINE" ]; then
            DNS=$DOMAINE
         else
            DNS=$ADRESSE_IP_PRIVEE:$PORT_RP
         fi
      fi
      openssl req -new -subj "/C=$CODE_PAYS/ST=$NOM_PAYS/L=$NOM_REGION/O=$NOM_ORGANISATION/CN=$DNS" -addext "subjectAltName = DNS:$DNS" -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -keyout "$DOSSIER_CERTS"/registry/"$DNS".key -out "$DOSSIER_CERTS"/registry/"$DNS".crt

      # Création des certificats client
      mkdir -p /etc/docker/certs.d/localhost:$PORT_REGISTRY
      mkdir -p /etc/docker/certs.d/"$DNS"
      cp "$DOSSIER_CERTS"/registry/localhost.crt /etc/docker/certs.d/localhost:$PORT_REGISTRY/
      cp "$DOSSIER_CERTS"/registry/"$DNS".crt /etc/docker/certs.d/"$DNS"/

      # Lancement du registry
      docker run -d -p $PORT_REGISTRY:443 --restart=always --name e-combox_registry -v "$DOSSIER_CERTS"/registry:/certs -v registry_data:/var/lib/registry -e VIRTUAL_HOST=registry -e REGISTRY_STORAGE_DELETE_ENABLED=true -e REGISTRY_HTTP_ADDR=0.0.0.0:443 -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/localhost.crt -e REGISTRY_HTTP_TLS_KEY=/certs/localhost.key -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 registry:latest
      # Connexion du registry au réseau de l'e-combox
      docker network disconnect bridge e-combox_registry 2>>/var/log/ecombox-error.log
      docker network connect bridge_e-combox e-combox_registry 2>>/var/log/ecombox-error.log
      echo -e "$COLINFO"
      echo -e "Le registry a été lancé.\n"
      echo -e "Le registry a été lancé." >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
      echo -e "$COLCMD"
   fi

   if docker network inspect bridge bridge_e-combox | grep e-combox_registry; then
      echo -e "$COLINFO"
      echo -e "Le réseau de l'e-combox est correctement rattaché au conteneur e-combox_registry."
      echo -e "$COLCMD"
   else
      echo -e "$COLINFO"
      echo -e "Rattachement du réseau de l'ecombox au conteneur e-combox_registry"
      echo -e "$COLCMD"
      docker network disconnect bridge e-combox_registry 2>>/var/log/ecombox-error.log
      docker network connect bridge_e-combox e-combox_registry 2>>/var/log/ecombox-error.log
   fi

   #Récupération du serveur git
   echo -e "$COLPARTIE"
   echo -e "\nLancement du serveur git"
   echo -e "$COLCMD"

   if docker ps | grep reseaucerta/git-http-server:"$TAG"; then
      echo -e "$COLINFO"
      echo -e "Rien à faire. Le serveur git local existe et est lancé.\n"
      echo -e "$COLCMD"
   else
      echo -e "$COLINFO"
      echo -e "Il n'y a pas de serveur git lancé. Nous allons procéder au lancement.\n"
      echo -e "$COLCMD"
      #if docker ps -a | grep reseaucerta/git-http-server:"$TAG" || docker ps | grep e-combox_gitserver; then
      if docker ps -a | grep e-combox_gitserver; then
         {
            docker stop e-combox_gitserver
            docker rm e-combox_gitserver
            docker volume rm e-combox_git-data
            docker run -d -p 443 --restart=always --name e-combox_gitserver -v e-combox_git-data:/git -e VIRTUAL_HOST=git -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 --network=bridge_e-combox reseaucerta/git-http-server:"$TAG"
         } 2>>/var/log/ecombox-error.log

         if [ $? != 0 ]; then
            echo -e "$COLINFO"
            echo -e "Le serveur git existait mais il a été supprimé et recréé car il n'était pas correctement lancé.\n. Il n'a toujours pas pas pu être lancé. Consultez les log d'erreur /var/log/ecombox-error.log."
            echo -e "$COLCMD"
            echo -e "Le serveur git existait mais il a été supprimé et recréé car il n'était pas correctement lancé.\n. Il n'a toujours pas pas pu être lancé. Consultez les log d'erreur /var/log/ecombox-error.log." >>/var/log/ecombox.log
            echo "" >>/var/log/ecombox.log
         else
            docker exec e-combox_gitserver crond &
            echo -e "$COLINFO"
            echo -e "Le serveur git existait mais il a été supprimé et recréé car il n'était pas correctement lancé. Il a correctement été lancé.\n"
            echo -e "$COLCMD"
            echo -e "Le serveur git existait mais il a été supprimé et recréé car il était soit mal en point, soit sur une mauvaise version. Il a correctement été lancé.\n" >>/var/log/ecombox.log
            echo "" >>/var/log/ecombox.log
         fi
      else
         docker run -d -p 443 --restart=always --name e-combox_gitserver -v e-combox_git-data:/git -e VIRTUAL_HOST=git -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 --network=bridge_e-combox reseaucerta/git-http-server:"$TAG" 2>>/var/log/ecombox-error.log
         if [ $? != 0 ]; then
            echo -e "$COLINFO"
            echo -e "Le serveur git n'a pas été lancé. Consultez les log d'erreur /var/log/ecombox-error.log."
            echo -e "$COLCMD"
            echo -e "Le serveur git n'a pas été lancé. Consultez les log d'erreur /var/log/ecombox-error.log" >>/var/log/ecombox.log
            echo "" >>/var/log/ecombox.log
         else
            docker exec e-combox_gitserver crond &
            echo -e "$COLINFO"
            echo -e "Le Git a été lancé.\n"
            echo -e "Le serveur Git a été lancé." >>/var/log/ecombox.log
            echo "" >>/var/log/ecombox.log
         fi
      fi
   fi

   # Suppression éventuelle des paramètres du proxy avant le démarrage de Portainer (il ne faut pas que Portainer démarre avec un proxy)
   if [ -e ~/.docker/config.json ]; then
      sed -i.bak "/\"proxies\": {/,9d" ~/.docker/config.json
      if [ "$(sed -n '$=' /root/.docker/config.json)" -le 3 ]; then
         echo "Le fichier config.json est supprimé pour Portainer."
         echo "Le fichier config.json est supprimé pour Portainer." >>/var/log/ecombox.log
         rm -rf ~/.docker/config.json
      fi
   fi

   # Portainer

   #Récupération de portainer
   echo -e "$COLPARTIE"
   echo -e "Récupération et configuration de Portainer"
   echo -e "$COLCMD"

   if [ -d "$DOSSIER_INSTALL/e-comBox_portainer" ]; then
      echo -e "$COLINFO"
      echo "Portainer existe et va être remplacé."
      echo -e "$COLCMD\c"
      echo -e "Portainer existe et va être remplacé." >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log

      cd $DOSSIER_INSTALL/e-comBox_portainer || exit
      docker-compose down
      cd ..
      rm -rf $DOSSIER_INSTALL/e-comBox_portainer
      #docker volume rm e-combox_portainer_portainer-data 2>> /var/log/ecombox-error.log
   fi

   cd $DOSSIER_INSTALL || exit
   git clone -b $BRANCHE "$DEPOT_GITLAB_PORTAINER".git 2>>/var/log/ecombox-error.log

   #Configuration de l'adresse IP
   echo -e "$COLINFO"
   echo -e "Mise à jour de $DOSSIER_INSTALL/e-comBox_portainer/.env."
   echo -e "$COLCMD"

   echo -e "$COLCMD\c"
   echo -e "PORT=$PORT_PORTAINER" >$DOSSIER_INSTALL/e-comBox_portainer/.env

   echo ""

   # Lancement de Portainer
   echo -e "$COLPARTIE"
   echo -e "Lancement de portainer"
   echo -e "$COLCMD\c"
   cd $DOSSIER_INSTALL/e-comBox_portainer/ || exit
   # On force le téléchargement de Portainer dans la dernière version (en dev uniquement)
   #docker pull portainer/portainer-ce:latest
   docker-compose up -d 2>>/var/log/ecombox-error.log

   if [ $? != 0 ]; then
      echo -e "$COLINFO"
      echo -e "Portainer n'a pas pu être lancé. Consultez les log d'erreur /var/log/ecombox-error.log."
      echo -e "$COLCMD"
      echo -e "Portainer n'a pas pu être lancé. Consultez les log d'erreur /var/log/ecombox-error.log." >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
   else
      echo -e "$COLINFO"
      echo -e "Portainer a été lancé. "
      echo -e "$COLCMD"
      echo -e "Portainer a été lancé. " >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
   fi

   # Attente pour que Portainer soit complètement opérationnel
   sleep 5

   # Modification du mot de passe de Portainer (si le mdp de Portainer est le mdp par défaut on demande à l'administrateur de le changer)

   # Test du mot de passe en comparaison avec le mdp par défaut (portnairAdmin)
   TEST_MDP_PORTAINER=$(curl --noproxy "*" -s -k -X POST -H "Content-Type: application/json;charset=UTF-8" -d "{\"username\":\"admin\",\"password\":\"portnairAdmin\"}" "https://localhost:$PORT_PORTAINER/api/auth")

   # Si le test précédent renvoie un "Invalid credential", c'est que le mdp a été changé
   if (echo "$TEST_MDP_PORTAINER" | grep "Invalid credentials" &>/dev/null); then
      echo -e "\nLe mot de passe du compte \"admin\" de Portainer a déjà été modifié.\n" >>/var/log/ecombox.log
   else
      # Connexion à l'API de Portainer
      CONNECTE_API

      # Saisie du nuveau mot de passe
      echo -e "$COLINFO"
      echo -e "\nLe mot de passe du compte \"admin\" de Portainer doit être modifié.\n"
      echo -e "$COLSAISIE"
      echo -e "Saisissez le nouveau mot de passe du compte \"admin\" de Portainer. Ce dernier doit être composé de 12 caractères au minimum (c'est normal que les lettres/chiffres/symboles saisis n'apparaissent pas)."
      SAISIRMDP

      # Mise à jour du mot de passe
      MAJ_MDP_PORTAINER=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/users/1/passwd" \
            -X PUT \
            -H "Authorization: Bearer $T" \
            -d "{\"newPassword\":\"$P_PASS\",\"password\":\"portnairAdmin\"}"
      )

      echo -e "Retour MAJ_MDP_PORTAINER : $MAJ_MDP_PORTAINER" >>/var/log/ecombox.log

   fi

   # Connexion à l'API pour le reste de l'installation
   CONNECTE_API

   # Réintégration du config.json

   if [ -e ~/.docker/config.json.bak ]; then
      mv ~/.docker/config.json.bak ~/.docker/config.json
   fi

   # Configuration de l'application

   echo -e "$COLPARTIE"
   echo -e "Mise à jour d'e-comBox si une version existe"
   echo -e "$COLCMD"

   if docker ps -a | grep reseaucerta/e-combox; then
      {
         docker rm -f e-combox
         docker volume rm ecombox_data
         docker volume rm ecombox_config
         docker volume rm ecombox_conf_nginx
      } 2>>/var/log/ecombox-error.log

      if docker ps -a | grep reseaucerta/e-combox:"$VERSION_APPLI"; then
         docker image rm -f reseaucerta/e-combox:"$VERSION_APPLI"
         echo -e "e-comBox existe et va être remplacée.\n" >>/var/log/ecombox.log
      fi
   fi

   # Récupération d'une éventuelle nouvelle version d'e-comBox
   echo -e "$COLPARTIE"
   echo "Récupération d'e-combox"
   echo -e "$COLCMD\c"
   echo -e ""

   docker pull reseaucerta/e-combox:"$VERSION_APPLI" 2>>/var/log/ecombox-error.log

   # Lancement de e-comBox
   echo -e "$COLPARTIE"
   echo "Lancement et configuration de l'environnement de l'application e-comBox"
   echo -e "$COLCMD\c"
   echo -e ""

   #Installation de l'e-combox avec le serveur Web Nginx
   docker run -dit --name e-combox -v ecombox_conf_nginx:/etc/nginx/conf.d -v ecombox_data:/usr/share/nginx/html/ -v ecombox_config:/etc/ecombox-conf -e VIRTUAL_PROTO=https -e VIRTUAL_PORT=443 -e VIRTUAL_HOST=app --restart always -p "$PORT_ECB":443 --network bridge_e-combox reseaucerta/e-combox:"$VERSION_APPLI" 2>>/var/log/ecombox-error.log

   if [ $? != 0 ]; then
      echo -e "$COLINFO"
      echo -e "L'application n'a pas pu être lancée. Consultez les log d'erreur /var/log/ecombox-error.log."
      echo -e "$COLCMD"
      echo -e "L'application n'a pas pu être lancée. Consultez les log d'erreur /var/log/ecombox-error.log." >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
   else
      echo -e "$COLINFO"
      echo -e "L'application a été lancée. "
      echo -e "$COLCMD"
      echo -e "L'application a été lancée. " >>/var/log/ecombox.log
      echo "" >>/var/log/ecombox.log
   fi

   # Dans tous les cas, il faut passer le reverse proxy de l'e-comBox, il faut donc modifier le fichier index.html et d'autres pour y introduire le chemin d'accès
   # La variable $CHEMIN_ABSOLU est vide si aucun chemin n'a été défini sinon elle contient le chemin avec un / devant

   sed -i "s|<base href=\"/\">|<base href=\"$CHEMIN_ABSOLU/app/\">|g" /var/lib/docker/volumes/ecombox_data/_data/index.html
   sed -i "s|script src=\"|script src=\"$CHEMIN_ABSOLU/app/|g" /var/lib/docker/volumes/ecombox_data/_data/index.html

   for fichier in /var/lib/docker/volumes/ecombox_data/_data/*.js; do
      sed -i "s|url('|url('$CHEMIN_ABSOLU/app/|g" "$fichier"
   done

   for fichier in /var/lib/docker/volumes/ecombox_data/_data/runtime-es*; do
      sed -i "s|__webpack_require__.p = \"\";|__webpack_require__.p = \"$CHEMIN_ABSOLU/app/\";|g" "$fichier"
   done

   # Configuration de l'API
   echo -e "$COLPARTIE"
   echo "Configuration d'e-comBox"
   echo -e "$COLCMD\c"
   echo -e ""

   if [ "$(grep -ni "API_URL" /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js | grep 'https' | cut -d"/" -f6)" ]; then
      PARTIE1=$(grep -ni "API_URL" /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js | grep 'https' | cut -d"/" -f3)
      PARTIE2=$(grep -ni "API_URL" /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js | grep 'https' | cut -d"/" -f4)
      ANCIENNE_URL=$PARTIE1/$PARTIE2
   else
      ANCIENNE_URL=$(grep -ni "API_URL" /var/lib/docker/volumes/ecombox_data/_data/main-es2015.js | grep 'https' | cut -d"/" -f3)
   fi

   # Détermination des nouvelles URL

   NOUVELLE_URL=$FQDN
   NOUVELLE_URL_PORTAINER=$FQDN/portainer/
   NOUVELLE_URL_APP=$FQDN/app/
   NOUVELLE_URL_SITE=$FQDN/nom_du_site/

   if [ "$ANCIENNE_URL" != "$NOUVELLE_URL" ]; then
      echo -e ""
      echo -e "L'URL $ANCIENNE_URL est remplacée par $NOUVELLE_URL." >>/var/log/ecombox.log

      for fichier in /var/lib/docker/volumes/ecombox_data/_data/*.js /var/lib/docker/volumes/ecombox_data/_data/*.js.map; do
         sed -i -e "s|$ANCIENNE_URL/|$NOUVELLE_URL_PORTAINER|g" "$fichier"
      done
   else
      echo -e ""
      echo -e "Aucun changement à opérer au niveau de $ANCIENNE_URL." >>/var/log/ecombox.log
   fi

   # Nettoyage éventuelle des anciennes images
   echo -e "Voulez-vous supprimer les images afin de gagner de l'espace (n par défaut) (${COLCHOIX}o/n${COLTXT}) ? ${COLSAISIE}\c"
   read -r REP_SUP
   if [ -z "$REP_SUP" ]; then
      REP_SUP="n"
   fi
   if [ "$REP_SUP" = "o" ]; then
      echo -e "$COLPARTIE"
      echo "Suppression des images si elles ne sont associées à aucun site"
      echo -e "$COLCMD\c"
      echo -e ""
      for image in $(docker images --format "{{.Repository}}:{{.Tag}}"); do
         docker rmi "$image"
      done 2>>/var/log/ecombox-error.log
   fi

   # Suppression des images inutilisées
   if [ "$(docker images -qf dangling=true)" ]; then
      docker rmi "$(docker images -qf dangling=true)" 2>>/var/log/ecombox-error.log
   fi

   # Arrêt des stacks
   if [ ! -e $DOSSIER_INSTALL/version2 ]; then
      echo -e "$COLINFO"
      echo "Les stacks doivent être obligatoirement redémarrés après reconfiguration de l'environnement, ils sont donc arrêtés."
      STOP_STACKS
   fi

   # Gestion de l'équipe Profs (voir si on lance ce script alors qu'un groupe Profs existe déjà)
   echo -e "$COLPARTIE"
   echo -e "Gestion éventuelle de l'équipe \"Profs\""
   echo -e "Gestion éventuelle de l'équipe \"Profs\"" >>/var/log/ecombox.log

   GESTION_PROFS

   # Ajout du stack (s'il n'existe pas) pour les mentions légales : ce stack doit uniquement accueillir les fichiers nécessaires.
   STACK_FSSERVER=$(curl --noproxy "*" -s -k -H "Authorization: Bearer $T" "$P_URL/api/stacks" | jq -c '.[] | select(.Name == "fsserver")')
   if [ -n "$STACK_FSSERVER" ]; then
      # Le stack a déjà été créé (même s'il n'est pas démarré)
      echo -e "$COLINFO"
      echo -e "Le stack FSserver a déjà été créé."
      echo -e "Le stack FSserver a déjà été créé." >>/var/log/ecombox.log
   else
      # le stack n'existe pas, il faut le créer
      echo -e "$COLCMD"
      echo "Création du stack \"FSserver\"..."
      echo "Création du stack \"FSserver\"..." >>/var/log/ecombox.log
      CREATE_STACK_FS=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/stacks?type=2&method=repository&endpointId=1" \
            -X POST \
            -H "Authorization: Bearer $T" -H "Content-Type: application/json" \
            -d "{\"name\": \"FSserver\",\"composeFile\": \"docker-compose-FSserver.yml\",\"repositoryURL\": \"$DEPOT_GITLAB_DC\",\"repositoryReferenceName\": \"refs/heads/$BRANCHE\",\"repositoryAuthentication\": false}"
      )
      STACK_FSSERVER=$CREATE_STACK_FS
      echo -e "$COLINFO"
      echo "Retour de la création du stack : $CREATE_STACK_FS" >>/var/log/ecombox.log
   fi

   # Démarrage du stack via l'api portainer si celui-ci n'est pas déjà lancé
   status_stack=$(echo "$STACK_FSSERVER" | jq -r '.Status')
   ID_STACK_FSSERVER=$(echo "$STACK_FSSERVER" | jq -r '.Id')

   if [ "$status_stack" = "1" ]; then
      # le stack est démarré
      echo -e "$COLINFO"
      echo -e "Le stack FSserver est démarré."
      echo -e "Le stack FSserver est démarré." >>/var/log/ecombox.log
   else
      # le stack n'est pas démarré, il sera démarré via l'api de portainer
      echo -e "$COLINFO"
      echo -e "Le satck FSserver n'est pas démarré, il faut le démarrer..."
      echo -e "Le satck FSserver n'est pas démarré, il faut le démarrer..." >>/var/log/ecombox.log
      echo -e "$COLCMD"
      echo "Démarrage du stack \"FSserver\"..."
      echo "Démarrage du stack \"FSserver\"..." >>/var/log/ecombox.log
      START_STACK_FSSERVER=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/stacks/$ID_STACK_FSSERVER/start" \
            -X POST \
            -H "Authorization: Bearer $T"
      )
      echo -e "$COLINFO"
      echo "Retour du démarrage du stack FSserver: $START_STACK_FSSERVER" >>/var/log/ecombox.log
   fi

   # Création d'une nouvelle restriction (association du stack au groupe Profs) si aucune restriction n'existe (sinon mis à jour de la restriction)
   RESTRICTION_FSSERVER=$(curl --noproxy "*" -s -k \
      "$P_URL/api/stacks/$ID_STACK_FSSERVER" \
      -X GET \
      -H "Authorization: Bearer $T" | grep ResourceControl)

   if [ -z "$RESTRICTION_FSSERVER" ]; then
      AJOUT_RESTRICTION_FSSERVER=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/resource_controls" \
            -X POST \
            -H "Authorization: Bearer $T" \
            -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6,\"resourceId\":\"1_fsserver\"}"
      )

      echo -e "Retour AJOUT_RESTRICTION_FSSERVER : $AJOUT_RESTRICTION_FSSERVER" >>/var/log/ecombox.log

   else
      # Récupération de l'ID de restriction
      ID_RESTRICTION_FSSERVER=$(echo "$RESTRICTION_FSSERVER" | jq -r '.ResourceControl.Id')
      # Mise à jour de la restriction
      MAJ_RESTRICTION_FSSERVER=$(
         curl --noproxy "*" -s -k \
            "$P_URL/api/resource_controls/$ID_RESTRICTION_FSSERVER" \
            -X PUT \
            -H "Authorization: Bearer $T" \
            -d "{\"administratorsOnly\":false,\"public\":false,\"teams\":[$ID_GROUPE_PROFS],\"type\":6}"
      )
      echo -e "Retour MAJ_RESTRICTION_FSSERVER : $MAJ_RESTRICTION_FSSERVER" >>/var/log/ecombox.log

   fi

   echo -e "$COLTITRE"
   echo "***************************************************"
   echo "*        FIN DE L'INSTALLATION DE E-COMBOX        *"
   echo "***************************************************"

   echo -e "$COLDEFAUT"
   echo "Téléchargement du fichier contenant les identifiants d'accès et des scripts permettant de reconfigurer l'application si nécessaire"
   echo -e "$COLCMD\c"

   # Téléchargement du fichier contenant les identifiants d'accès, du CHANGELOG.md et les scripts utiles à la configuration
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/e-comBox_identifiants_acces_applications.pdf -o $DOSSIER_INSTALL/e-comBox_identifiants_acces_applications.pdf
   curl -fsSL https://gitlab.com/e-combox/e-combox_webapp/-/raw/master/CHANGELOG.md -o $DOSSIER_INSTALL/CHANGELOG.md
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/configure_application.sh -o $DOSSIER_INSTALL/configure_application.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/sauv_sites.sh -o $DOSSIER_INSTALL/sauv_sites.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/restaure_sites.sh -o $DOSSIER_INSTALL/restaure_sites.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/restaure_v3.sh -o $DOSSIER_INSTALL/restaure_v3.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/delete_conteneurs.sh -o $DOSSIER_INSTALL/delete_conteneurs.sh
   curl -fsSL $DEPOT_GITLAB_SCRIPTS/raw/$BRANCHE/reset_pass_portainer.sh -o $DOSSIER_INSTALL/reset_pass_portainer.sh

   # Migration des sites si la v3 est installée

   if [ -e $DOSSIER_INSTALL/version3 ]; then
      # Migration des sites
      echo -e "$COLTITRE"
      echo "***************************************************"
      echo "*              MIGRATION DES SITES                *"
      echo "***************************************************"
      echo -e "$COLDEFAUT"
      UPDATE_STACKS
      echo -e "$COLINFO"
      echo -e "Les sites ont été mis à jour."

      # Suppression du fichier témoin
      rm $DOSSIER_INSTALL/version3

      # Suppression des images de la version 3
      echo "Suppression des images de la version 3."
      echo -e "$COLCMD\c"
      echo -e ""
      for image in $(docker images --filter=reference='reseaucerta/*:3.0' --format "{{.Repository}}:{{.Tag}}"); do
         docker rmi "$image"
      done 2>>/var/log/ecombox-error.log

      # Suppression des images locales personnalisées
      echo "Suppression des images locales personnalisées."
      echo -e "$COLCMD\c"
      echo -e ""
      for image in $(docker images --filter=reference='localhost:*/*' --format "{{.Repository}}:{{.Tag}}"); do
         docker rmi "$image"
      done 2>>/var/log/ecombox-error.log

      # Suppression des images inutilisées
      if [ "$(docker images -qf dangling=true)" ]; then
         docker rmi "$(docker images -qf dangling=true)" 2>>/var/log/ecombox-error.log
      fi

      # Ménage des scripts devenus inutiles
      rm "$DOSSIER_INSTALL"/ajout_auth.sh "$DOSSIER_INSTALL"/suppr_auth.sh "$DOSSIER_INSTALL"/restaure_v2.sh "$DOSSIER_INSTALL"/change_config_ip.sh "$DOSSIER_INSTALL"/migrate_application.sh "$DOSSIER_INSTALL"/sync_pwd_portainer.sh
   fi

   # Pour permettre aux utilisateurs non admin de créer des stacks (option qui change à la migration)
   MODIF_SETTINGS_PORTAINER=$(
      curl --noproxy "*" -s -k \
         "$P_URL/api/endpoints/1/settings" \
         -H "Authorization: Bearer $T" -X PUT \
         -H "Content-Type: application/json;charset=UTF-8" \
         -H 'Cache-Control: no-cache' \
         -d "{\"allowSysctlSettingForRegularUsers\":true}"
   )
   echo -e "Retour MODIF_SETTINGS_PORTAINER : $MODIF_SETTINGS_PORTAINER" >>/var/log/ecombox.log

   # Affichage des URL
   echo -e "$COLINFO"
   echo -e "L'application e-comBox est maintenant accessible à l'URL suivante :"
   echo -e "${COLCHOIX}https://$NOUVELLE_URL_APP"
   echo -e "$COLINFO"
   echo "Portainer est accessible à l'URL suivante :"
   echo -e "${COLCHOIX}https://$NOUVELLE_URL_PORTAINER"
   echo -e "$COLINFO"
   echo "Les sites seront accessibles via des URL formées de la manière suivante :"
   echo -e "${COLCHOIX}https://$NOUVELLE_URL_SITE"

   echo -e "$COLCMD\n"
   echo -e "Les identifiants d'accès aux applications de l'e-comBox figurent dans le fichier $DOSSIER_INSTALL/e-comBox_identifiants_acces_applications.pdf."
   echo -e ""
   echo -e "Vous pouvez accéder à l'interface d'e-comBox via le compte admin de Portainer mais une bonne pratique consiste à créer, sur Portainer, au moins un compte dans le groupe \"Profs\" puis accéder à l'interface d'e-comBox avec les identifiants de ce dernier."
   echo -e ""

   echo -e "$COLCMD"
}

verifVersion() {
   # Vérification d'anciennes versions
   if docker ps -a | grep e-combox:1.0; then
      echo -e "$COLINFO"
      echo -e "**** ATTENTION ****"
      echo -e "Le système constate que la version 1 est installée. La version des sites est incompatible, ces derniers seront supprimés."
      POURSUIVRE
      # Nettoyage complet des éléments de Docker
      CONTENEURS_ACTIFS=$(docker ps -q)
      if [ -n "$CONTENEURS_ACTIFS" ]; then
         docker stop "$CONTENEURS_ACTIFS" 2>>/var/log/ecombox_v1-error.log
      fi
      docker system prune -f -a --volumes 2>>/var/log/ecombox_v1-error.log
      echo -e "$COLTXT"
      echo -e "Suppression des sites effectués. Le système va procéder à la mise à jour de l'e-combox vers la version 4."
      installer
   else
      if docker ps -a | grep e-combox:2.0 >>/var/log/ecombox-error.log; then
         echo -e "$COLINFO"
         echo -e "**** ATTENTION ****"
         echo -e ""
         echo -e "Le système constate que la version 2 est installée. Installez la version 3 avant de passer à la version 4."
         exit
      else
         if docker ps -a | grep e-combox:3.0 >>/var/log/ecombox-error.log; then
            echo -e "$COLINFO"
            echo -e "**** ATTENTION ****"
            echo -e ""
            echo -e "Le système constate que la version 3 est installée. Une migration des sites est nécessaire, celle-ci est sans danger."
            echo -e "Mais par mesure de précaution, assurez-vous de disposer d'une sauvegarde de la v3. Si ce n'est pas le cas, il est conseillé de ne pas poursuivre, de réaliser la sauvegarde (/opt/e-comBox/sauv_sites.sh) et de relancer ce script."
            POURSUIVRE
            touch $DOSSIER_INSTALL/version3
            installer
         else
            installer

         fi
      fi
   fi
}

initInstall
