#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# Configuration de l'interface d'authentification

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel du fichier de fonctions (qui fait lui-même appel au fichier de variable et à param.conf)
source $DOSSIER_INSTALL/fonctions.sh

SAISIRMDP() {

   read -rs MDP
   if [ -n "$MDP" ]; then
      echo -e "$COLSAISIE\n"
      echo -e "Saisissez de nouveau le mot de passe."
      read -rs MDPbis

      while [ "$MDP" != "$MDPbis" ]; do
         echo -e "$COLSTOP\n"
         echo -e "Les mots de passe ne correspondent pas. Il faut recommencer."
         echo -e "$COLSAISIE\n"
         echo -e "Saisissez le mot de passe."
         read -rs MDP
         echo ""
         echo -e "Saisissez de nouveau le mot de passe."
         read -rs MDPbis
         echo ""
      done
   fi
}

echo -e "$COLTITRE"
echo "***************************************************"
echo "*       CONFIGURATION DE L'AUTHENTIFICATION       *"
echo "***************************************************"

echo -e "$COLDEFAUT\n"

if [ ! -e /var/lib/docker/volumes/ecombox_config/_data/.auth ]; then
   echo -e "Vous vous apprếtez à configurer un mot de passe pour accéder à e-comBox. Ce dernier sera associé à un compte \"admin\" préalablement créé. $COLSAISIE\n"
   echo -e "Saisissez le mot de passe (le mot de passe saisi ne s'affiche pas) ou laisser vide pour continuer sans authentification."
   SAISIRMDP
else
   echo -e "Vous vous apprếtez à modifier le mot de passe associé au compte \"admin\". $COLSAISIE\n"
   echo -e "Saisissez un nouveau mot de passe (le mot de passe saisi ne s'affiche pas) ou laissez vide pour continuer avec l'ancien."
   SAISIRMDP
fi

echo -e "$COLCMD"

if [ -z "$MDP" ]; then
   STOPPER_AJOUT_AUTH
else
   sed -ir "/try_files \$uri \/index.html;/aauth_basic_user_file \/etc\/ecombox-conf\/.auth;" /var/lib/docker/volumes/ecombox_conf_nginx/_data/default.conf
   sed -ir "/try_files \$uri \/index.html;/aauth_basic \"Authentification E-COMBOX\";" /var/lib/docker/volumes/ecombox_conf_nginx/_data/default.conf
   docker exec e-combox nginx -s reload

   if [ ! -e /var/lib/docker/volumes/ecombox_config/_data/.auth ]; then
      docker exec e-combox htpasswd -cBb /etc/ecombox-conf/.auth admin "$MDP"
   else
      docker exec e-combox htpasswd -Bb /etc/ecombox-conf/.auth admin "$MDP"
   fi
fi

echo -e "$COLINFO\n"
echo -e "L'authentification pour accéder à l'interface d'e-comBox a été réalisée."
echo -e ""
echo -e "Vous pouvez la supprimer en exécutant la commande suivante : bash /opt/e-comBox/suppr_auth.sh"
echo -e "$COLCMD"

exit 0