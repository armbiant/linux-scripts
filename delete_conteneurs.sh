#!/bin/bash
# shellcheck disable=SC2181,SC2001
# shellcheck source=/dev/null

# suppression de tous les conteneurs et relance de l'installation

# Définition du dossier d'installation
DOSSIER_INSTALL=/opt/e-comBox

# Appel du fichier de fonctions (qui fait lui-même appel au fichier de variable et à param.conf)
source $DOSSIER_INSTALL/fonctions.sh

echo -e "$COLTXT"
echo -e "Si vous poursuivez l'exécution de ce script, tous vos sites seront supprimés et vos données seront perdues."
POURSUIVRE
echo -e "$COLCMD"

# Suppression des conteneurs, volumes, réseaux et images
STOP_STACKS
#shellcheck disable=SC2046
docker stop $(docker ps -aq)
docker system prune -a -f --volumes

# Suppression des dossiers
rm -rf $DOSSIER_INSTALL/e-comBox_portainer
rm -rf $DOSSIER_INSTALL/e-comBox_reverseproxy

echo -e "$COLTXT"
echo -e "Si vous poursuivez l'exécution de ce script, l'application va être installée avec ses paramètres d'origine."
POURSUIVRE
echo -e "$COLCMD"
bash $DOSSIER_INSTALL/configure_application.sh
