#!/bin/bash
# Ce script lance des scripts qui automatise l'installation de Docker et Docker-Compose
# shellcheck disable=SC2034

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLINFO="\033[0;36m"    # Cyan

clear
echo -e "$COLTITRE"
echo "************************************************************"
echo "*         INSTALLATION DE DOCKER ET DOCKER-COMPOSE         *"
echo "************************************************************"



# Pour l'installation de Docker
# Utilisation du script officiel fourni par Docker 
# https://github.com/docker/docker-install pour Docker

echo -e ""
echo -e "$COLPARTIE"
echo -e "Installation de Docker"
echo -e ""

echo -e "$COLCMD"

apt-get update && apt-get install -y curl git
apt-get install -y apt-transport-https ca-certificates gnupg2 software-properties-common
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

echo -e ""
echo -e "$COLINFO"
echo -e "Docker est installé"
echo -e ""

echo -e "$COLCMD"


# Installation de Docker Compose
echo -e ""
echo -e "$COLPARTIE"
echo -e "Installation de Docker-Compose"
echo -e ""

echo -e "$COLCMD"

# Installation des dépendances
#apt install -y python-backports.ssl-match-hostname python-cached-property python-docker python-dockerpty python-docopt python-functools32 python-jsonschema python-texttable python-websocket python-yaml git

# Installation de la dernière version du docker-compose (selon les systèmes, va chercher dans /usr/local/bin ou /usr/bin)
#wget -N --output-document=/usr/bin/docker-compose "https://github.com/docker/compose/releases/download/$(wget --quiet --output-document=- https://api.github.com/repos/docker/compose/releases/latest | grep --perl-regexp --only-matching '"tag_name": "\K.*?(?=")')/run.sh"
#chmod +x /usr/bin/docker-compose
#wget -N --output-document=/usr/local/bin/docker-compose "https://github.com/docker/compose/releases/download/$(wget --quiet --output-document=- https://api.github.com/repos/docker/compose/releases/latest | grep --perl-regexp --only-matching '"tag_name": "\K.*?(?=")')/run.sh"
#chmod +x /usr/local/bin/docker-compose
#wget -N --output-document=/etc/bash_completion.d/docker-compose "https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose"
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

echo -e ""
echo -e "$COLINFO"
echo -e "Docker-Compose est installé"
echo -e ""

echo -e "$COLCMD"


